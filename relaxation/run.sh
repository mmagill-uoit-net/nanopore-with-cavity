#!/bin/bash



# 7 values of N
# 10 seeds
# 70 cases

# Post-equilibration runtime
T=10000

for seed_prefix in {10..19}
do
    rseed=${seed_prefix}${N}1234567

    N=10
    time=200m
    label=${N}_${rseed}
    sqsub -q serial -o logs/log_${label}.log -r $time ~/espresso-2.1.2j/Espresso relaxation-for-nanopore-with-cavity.tcl $N $T 0 $rseed

    N=25
    time=400m
    label=${N}_${rseed}
    sqsub -q serial -o logs/log_${label}.log -r $time ~/espresso-2.1.2j/Espresso relaxation-for-nanopore-with-cavity.tcl $N $T 0 $rseed

    N=50
    time=1000m
    label=${N}_${rseed}
    sqsub -q serial -o logs/log_${label}.log -r $time ~/espresso-2.1.2j/Espresso relaxation-for-nanopore-with-cavity.tcl $N $T 0 $rseed

    N=75
    time=2000m
    label=${N}_${rseed}
    sqsub -q serial -o logs/log_${label}.log -r $time ~/espresso-2.1.2j/Espresso relaxation-for-nanopore-with-cavity.tcl $N $T 0 $rseed

    N=100
    time=3000m
    label=${N}_${rseed}
    sqsub -q serial -o logs/log_${label}.log -r $time ~/espresso-2.1.2j/Espresso relaxation-for-nanopore-with-cavity.tcl $N $T 0 $rseed

    N=150
    time=5000m
    label=${N}_${rseed}
    sqsub -q serial -o logs/log_${label}.log -r $time ~/espresso-2.1.2j/Espresso relaxation-for-nanopore-with-cavity.tcl $N $T 0 $rseed

    N=200
    time=10000m
    label=${N}_${rseed}
    sqsub -q serial -o logs/log_${label}.log -r $time ~/espresso-2.1.2j/Espresso relaxation-for-nanopore-with-cavity.tcl $N $T 0 $rseed

done
