import numpy as np
import matplotlib.pyplot as plt


Ns = np.array([25,50,100,200])
#Ns = [25]

Ntaus = 2000

fitMins = [20,50,100,400]
fitMaxs = [50,150,300,1000]

tauRs = np.zeros(len(Ns))

for i in range(len(Ns)):
    N = Ns[i]

    # Load data
    LEs = np.loadtxt('data-bak/%d_123456789_LE.dat' % N)

    # Average LE
    mean_LE = np.mean(LEs)

    # Autocorrelation function
    R = np.zeros(Ntaus)
    for tau in range(Ntaus):

        # Roll forward, then remove first tau bins
        correlation = (LEs - mean_LE) * (np.roll(LEs,tau) - mean_LE)
        correlation = correlation[tau:]

        # Correlation function is the average
        R[tau] = np.mean(correlation)

    # Fit exponential: resolution happens to be good up to about tau = N
    taus = np.arange(np.shape(R)[0],dtype=float)
    myfit = np.polyfit(taus[fitMins[i]:fitMaxs[i]], np.log(R[fitMins[i]:fitMaxs[i]]), 1)
    mypol = np.poly1d(myfit)
    tauR = -1./myfit[0]
    tauRs[i] = tauR

    # tau ~ 2*N is about the desired plotting range (coincidence again)
    plt.semilogy(R,label='Autocorrelation')
    plt.semilogy(np.exp(mypol(taus)),label='tau_R ~ %.1f' % tauR)
    plt.grid(which='both')
    plt.legend(loc='lower left')
    plt.savefig('Correlation_N%d.eps' % N)
    plt.clf()



# Plot relaxation times
plt.loglog(Ns,tauRs,'*-',label='Simulation')
Ns_fine = np.linspace(0,200,1000)
plt.loglog(Ns_fine,0.015*Ns_fine**(2*0.588 + 1),label=r'$0.015 N^{2\nu + 1}$')
plt.xlim([25,200])
plt.ylim([10,3000])
plt.xlabel('Chain Length')
plt.ylabel('Relaxation Time')
plt.grid()
plt.legend(loc='upper left',numpoints=1)
plt.show()
