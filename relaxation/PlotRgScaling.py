import numpy as np
import matplotlib.pyplot as plt
import glob
import matplotlib

# Fontsizes
matplotlib.rcParams['svg.fonttype'] = 'none'
matplotlib.rcParams['font.family'] = 'sans-serif'
fontsize_innerlabels=20
fontsize_axislabels=26
fontsize_ticks=16
matplotlib.rcParams['xtick.labelsize'] = fontsize_ticks
matplotlib.rcParams['ytick.labelsize'] = fontsize_ticks


LoadedData = np.loadtxt('mean_RGs.txt')
Ns = LoadedData[0,:]
mean_RGs = LoadedData[1,:]

# Filter
Ns = Ns[2:-1]
mean_RGs = mean_RGs[2:-1]


# Fit
myfit = np.polyfit(np.log(Ns),np.log(mean_RGs),1)
mypol = np.poly1d(myfit)
print mypol[1]

plt.loglog(Ns,mean_RGs,'*',markersize=13)
plt.loglog(Ns,np.exp(mypol[0])*Ns**mypol[1],'k--')
#plt.loglog(Ns,1/np.sqrt(6)*Ns**0.588,'k--')

plt.text(1.2*Ns[0],1.2*mean_RGs[1],'$R_G \sim\, %.3f \,N^{\,%.3f}$'%(np.exp(mypol[0]),mypol[1]),fontsize=fontsize_innerlabels)
plt.xlim([0.9*Ns[0],1.1*Ns[-1]])
plt.ylim([0.9*mean_RGs[0],1.1*mean_RGs[-1]])
plt.xlabel('$N$',fontsize=fontsize_axislabels)
plt.ylabel('$R_G/\sigma$',fontsize=fontsize_axislabels)
extra_ticks = np.arange(50,150+1,25)
plt.gca().get_xaxis().set_ticks(extra_ticks)
plt.gca().get_xaxis().set_ticklabels(extra_ticks)
plt.gca().get_yaxis().set_ticks([5,6,7,8,9,10])
plt.gca().get_yaxis().set_ticklabels([5,6,7,8,9,10])
plt.subplots_adjust(left=0.14, bottom=0.14, right=0.95, top=0.98,
                     wspace=0.20, hspace=0.20)

plt.show()

