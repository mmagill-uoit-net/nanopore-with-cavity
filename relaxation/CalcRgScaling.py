import numpy as np
import matplotlib.pyplot as plt
import glob


Ns = np.array([10,25,50,75,100,150,200])
#Ns = np.array([25,50,100])
#Ns = [25]
mean_RGs = np.zeros((len(Ns),))

for i in range(len(Ns)):
    N = Ns[i]
    RGs = np.array([])

    for filename in glob.glob('data/%d_*_RG.dat' % N):

        # Load data
        RGs = np.concatenate((RGs,np.loadtxt(filename)))
	#plt.plot(RGs)
	#plt.show()

    # Average RG
    mean_RGs[i] = np.mean(RGs)

# Save mean_RGs
np.savetxt('mean_RGs.txt',(Ns,mean_RGs))

# Fit
myfit = np.polyfit(np.log(Ns-1),np.log(mean_RGs),1)
mypol = np.poly1d(myfit)
print mypol[1]

plt.loglog(Ns,mean_RGs,'*')
plt.loglog(Ns,np.exp(mypol[0])*Ns**mypol[1],'k--')
#plt.loglog(Ns,0.427*Ns**0.588,'k--')

plt.text(1.2*Ns[0],1.2*mean_RGs[1],'$R_G \sim\, %.3f \,N^{\,%.3f}$'%(np.exp(mypol[0]),mypol[1]),fontsize=22)
plt.xlim([0.9*Ns[0],1.1*Ns[-1]])
plt.ylim([0.9*mean_RGs[0],1.1*mean_RGs[-1]])
plt.xlabel('N')
plt.ylabel('R_g')
#plt.show()

