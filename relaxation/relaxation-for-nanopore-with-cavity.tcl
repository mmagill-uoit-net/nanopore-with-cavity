# Example input:
# ~/espresso-2.1.2j/Espresso relaxation-for-nanopore-with-cavity.tcl 20 100000 1 1234

# Parse input
set N            [lindex $argv 0]
set T            [lindex $argv 1]
set vis_flag     [lindex $argv 2]
set rseed        [lindex $argv 3]

########################################################################################

# Function runs a single translocation
global VMD_IS_ON 
set VMD_IS_ON 0
proc RunMain { N T vis_flag rseed } {
    global VMD_IS_ON 
    t_random seed    $rseed

    # Output end-to-end length, L_E
    set CaseName "${N}_${rseed}"
    set FLE   [open "data/${CaseName}_LE.dat"   "w"]
    set FRG   [open "data/${CaseName}_RG.dat"   "w"]

    # Scales
    set sig 1.0
    set eps 1.0

    # Domain size
    set box_l 400

    # Thermostat Parameters
    set temp 1.
    set gamma 1.

    # FENE Potential Parameters
    set kap [expr {30.0*$eps/($sig*$sig)}]
    set lam [expr {1.5*$sig}]

    # LJ Potential Parameters
    set cut   [expr {pow(2.0,1.0/6.0)*$sig}]
    set shift [expr {0.25*$eps}]


    ##########################################################################


    # Spatial domain creation
    setmd box_l $box_l $box_l $box_l

    # Temporal domain creation
    setmd time_step 0.01
    setmd skin 0.4

    # Interaction creations
    inter 0 fene $kap $lam
    inter 7 angle 0
    inter 0 0  lennard-jones $eps $sig $cut $shift 0.

    # Polymer creation
    set x [expr {$box_l/2.}]
    set y [expr {$box_l/2.}]
    set z [expr {$box_l/2.}]
    part 0 pos $x $y $z type 0 fix 0 0 0 ext_force 0. 0. 0.
    # Build the chain (without angular potential)
    for { set i 1 } { $i < $N } { incr i } {
	# Z position
	set z [expr {$z + $sig}]

	# Place the ith particle
	part $i pos $x $y $z type 0 fix 0 0 0 ext_force 0. 0. 0.

	# FENE bond to the previous particle in the chain
	part $i bond 0 [expr {$i - 1}] 
    }
    # After all particles are placed, apply the angular potential (if any)
    for { set i 1 } { $i < [expr {$N-1}] } { incr i } {
	part $i bond 7 [expr {$i - 1}] [expr {$i + 1}]
    }


    ##########################################################################


    # Initialize Visualization
    if { $vis_flag == 1 } {
	if { $VMD_IS_ON == 0 } {
	    prepare_vmd_connection vmdout
	    imd listen 100
	    set VMD_IS_ON 1
	}
	imd positions
    }

    # Equilibrate (fix all threaded monomers)
    puts "Equilibrating..."
    thermostat langevin $temp 0.1
    for {set i 1} {$i < 5e3} {incr i 1} {
	integrate 100
	if { $vis_flag == 1 } {
	    imd positions
	}
    }
    puts "Equilibrated"

    # Simulate
    thermostat langevin $temp $gamma
    set timestep 0
    while {$timestep < $T} {

	# Calculate L_E
	set LE [veclen [vecsub [part 0 print pos] [part [expr {$N-1}] print pos]]]

	# Calculate R_G
	set RG2 0
	for {set i 0} {$i < $N} {incr i 1} {
	    for {set j 0} {$j < $N} {incr j 1} {
		set rirj [veclen [vecsub [part $i print pos] [part $j print pos]]]
		set RG2 [expr {$RG2 + $rirj*$rirj}]
	    }
	}
	set RG2 [expr {$RG2/(2.0*$N*$N)}]
	set RG [expr {sqrt($RG2)}]

	# Integrate
	puts $FLE   "$LE"
	puts $FRG   "$RG"
	integrate 100
	incr timestep 1
	if { $vis_flag == 1 } {
	    imd positions
	}
    }

    close $FLE
    close $FRG
    return 0
}


##########################################################################
##########################################################################

# Actually run the program
RunMain $N $T $vis_flag $rseed


