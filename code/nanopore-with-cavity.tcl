# Example input:
# ~/espresso-2.1.2j/Espresso nanopore-with-cavity.tcl 20 4. 1. 0 10 1234

# Parse input (currently cis-trans symmetric and wall thickness of 1 sig)
set N            [lindex $argv 0]
set reff_cavi    [lindex $argv 1]
set force_pore   [lindex $argv 2]
set vis_flag     [lindex $argv 3]
set nevents      [lindex $argv 4]
set rseed        [lindex $argv 5]
t_random seed    $rseed

########################################################################################

# Function runs a single translocation
global VMD_IS_ON 
set VMD_IS_ON 0
proc RunMain { N reff_cavi force_pore vis_flag Fstate Ffail} {
    global VMD_IS_ON 

    # Scales
    set sig 1.0
    set eps 1.0

    # Domain size
    set box_l 400

    # Thermostat Parameters
    set temp 1.
    set gamma 1.

    # FENE Potential Parameters
    set kap [expr {30.0*$eps/($sig*$sig)}]
    set lam [expr {1.5*$sig}]

    # LJ Potential Parameters
    set cut   [expr {pow(2.0,1.0/6.0)*$sig}]
    set shift [expr {0.25*$eps}]


    # Geometry Parameters
    # All dimensions defined here are the effective dimensions
    set reff_cis    [expr {1.5 - 0.5*$sig}]
    set reff_trans  $reff_cis
    set teff_cavi   [expr {2.*$reff_cavi}]
    set teff_cis    1.0001
    set teff_trans  1.0001
    set force_cis   $force_pore
    set force_trans $force_pore
    set force_cavi  [expr {($reff_cis/$reff_cavi)**2 * $force_pore}]

    ##########################################################################


    # Spatial domain creation
    setmd box_l $box_l $box_l $box_l

    # Temporal domain creation
    setmd time_step 0.01
    setmd skin 0.4

    # Interaction creations
    inter 0 fene $kap $lam
    inter 7 angle 0
    inter 0 0  lennard-jones $eps $sig $cut $shift 0.
    inter 0 76 lennard-jones $eps $sig $cut $shift 0.

    # Pore creation (cis, trans, cavity)
    set rnom_cis   [expr {$reff_cis   + $sig/2.}]
    set rnom_cavi  [expr {$reff_cavi  + $sig/2.}]
    set rnom_trans [expr {$reff_trans + $sig/2.}]

    # Nominal wall thickness is ~0 so that monomer centers can get sigma/2 from the center of the wall
    set tnom_cis   [expr {($teff_cis   - $sig)}]
    set tnom_cavi  [expr {($teff_cavi  - $sig)}] 
    set tnom_trans [expr {($teff_trans - $sig)}]

    # The wall lengths are their half-thicknesses
    set leff_cis   [expr {$teff_cis  /2.}]
    set leff_cavi  [expr {$teff_cavi /2.}]
    set leff_trans [expr {$teff_trans/2.}]
    set lnom_cis   [expr {$tnom_cis  /2.}]
    set lnom_cavi  [expr {$tnom_cavi /2.}]
    set lnom_trans [expr {$tnom_trans/2.}]

    # Effective boundaries are in contact with one another (not nominal boundaries)
    set z0_cis      [expr {$box_l/2. - ($leff_cavi + $leff_cis  )}]
    set z0_cavi     [expr {$box_l/2.}]
    set z0_trans    [expr {$box_l/2. + ($leff_cavi + $leff_trans)}]

    constraint pore center [expr {$box_l/2.}] [expr {$box_l/2.}] [expr {$z0_cis}]   axis 0 0 1 radius $rnom_cis   length $lnom_cis   type 76
    constraint pore center [expr {$box_l/2.}] [expr {$box_l/2.}] [expr {$z0_cavi}]  axis 0 0 1 radius $rnom_cavi  length $lnom_cavi  type 76
    constraint pore center [expr {$box_l/2.}] [expr {$box_l/2.}] [expr {$z0_trans}] axis 0 0 1 radius $rnom_trans length $lnom_trans type 76

    # # Fake pore monomers
    # if { $vis_flag == 1 } {
    # 	# Number of fake monomers along each branch
    # 	set N_fakes 5
    # 	for { set i 0 } { $i < $N_fakes } { incr i } {
    # 	    # Jump index by N_fakes for each branch so indices don't overlap
    # 	    # Cis Pore
    # 	    part [expr {$N+$i+0*$N_fakes}] pos [expr {$box_l/2. + ($rnom_cis + $i*$sig)}]   [expr {$box_l/2.}] $z0_cis    type 1 fix 1 1 1 
    # 	    part [expr {$N+$i+1*$N_fakes}] pos [expr {$box_l/2. - ($rnom_cis + $i*$sig)}]   [expr {$box_l/2.}] $z0_cis    type 1 fix 1 1 1 
    # 	    # Trans Pore
    # 	    part [expr {$N+$i+2*$N_fakes}] pos [expr {$box_l/2. + ($rnom_trans + $i*$sig)}] [expr {$box_l/2.}] $z0_trans  type 1 fix 1 1 1 
    # 	    part [expr {$N+$i+3*$N_fakes}] pos [expr {$box_l/2. - ($rnom_trans + $i*$sig)}] [expr {$box_l/2.}] $z0_trans  type 1 fix 1 1 1 
    # 	    # Cavity Walls
    # 	    set cur_znom [expr {($z0_cavi - $lnom_cavi) + $i * ($tnom_cavi)/($N_fakes-1.) }]
    # 	    part [expr {$N+$i+4*$N_fakes}] pos [expr {$box_l/2. + ($rnom_cavi)}]            [expr {$box_l/2.}] $cur_znom type 1 fix 1 1 1 
    # 	    part [expr {$N+$i+5*$N_fakes}] pos [expr {$box_l/2. - ($rnom_cavi)}]            [expr {$box_l/2.}] $cur_znom type 1 fix 1 1 1 
    # 	}
    # }

    # Polymer creation (ncavi at time 0 is threads)
    # Need to offset from absolute center because potential is ill-defined (Espresso Bug)
    set threads 2
    set x [expr {$box_l/2. + 0.01}]
    set y [expr {$box_l/2. + 0.01}]
    set z [expr {$z0_cis + $threads*$sig + 0.01}]
    part 0 pos $x $y $z type 0 fix 0 0 0 ext_force 0. 0. 0.
    # Build the chain (without angular potential)
    for { set i 1 } { $i < $N } { incr i } {
	# Z position
	set z [expr {$z - $sig}]
	# Place the ith particle
	part $i pos $x $y $z type 0 fix 0 0 0 ext_force 0. 0. 0.
	# FENE bond to the previous particle in the chain
	part $i bond 0 [expr {$i - 1}] 
    }
    # After all particles are placed, apply the angular potential (if any)
    for { set i 1 } { $i < [expr {$N-1}] } { incr i } {
	part $i bond 7 [expr {$i - 1}] [expr {$i + 1}]
    }


    ##########################################################################


    # Initialize Visualization
    if { $vis_flag == 1 } {
	if { $VMD_IS_ON == 0 } {
	    prepare_vmd_connection vmdout
	    imd listen 100
	    set VMD_IS_ON 1
	}
	imd positions
    }

    # Equilibrate (fix all threaded monomers)
    puts "Equilibrating..."
    for { set i 0 } { $i <= $threads } { incr i } {
	part $i fix
    }
    thermostat langevin $temp 0.1
    for {set i 1} {$i < 1e3} {incr i 1} {
	integrate 100
	if { $vis_flag == 1 } {
	    imd positions
	}
    }
    puts "Equilibrated"
    for { set i 0 } { $i <= $threads } { incr i } {
	part $i unfix
    }

    # Simulate
    thermostat langevin $temp $gamma
    set ncis   0
    set ncavi  0
    set ntrans 0
    set timestep 0
    while {$ntrans < $N} {
	# Counters
	set ncis   0
	set ncavi  0
	set ntrans 0

	# Apply E-field
	for { set i 0 } { $i < $N } { incr i } {
	    # Current particle's z coordinate
	    set z [lindex [part $i print pos] 2]

	    # Update force based on location (cis, trans, cavity, none)
	    if       {$z <= [expr {$z0_cis   + $teff_cis/2.}]   &&  $z >= [expr {$z0_cis   - $teff_cis/2.}] }   { 
		# Cis Force
		part $i ext_force 0 0 $force_cis
	    } elseif {$z <= [expr {$z0_cavi  + $teff_cavi/2.}]  &&  $z >= [expr {$z0_cavi  - $teff_cavi/2.}] }  { 
		# Cavity Force
		part $i ext_force 0 0 $force_cavi
	    } elseif {$z <= [expr {$z0_trans + $teff_trans/2.}] &&  $z >= [expr {$z0_trans - $teff_trans/2.}] } { 
		# Trans Force
		part $i ext_force 0 0 $force_trans
	    } else {
		# Remove forces (if any)
		part $i ext_force 0 0 0
	    }
	}

	# Count Positions
	for { set i 0 } { $i < $N } { incr i } {
	    # Current particle's z coordinate
	    set z [lindex [part $i print pos] 2]

	    if       {$z <= $z0_cis}   { 
		incr ncis
	    } elseif {$z  > $z0_cis && $z < $z0_trans }  { 
		incr ncavi
	    } elseif {$z >= $z0_trans} { 
		incr ntrans
	    }
	}

	# If event failed, exit function with error message
	if { $ncis == $N } {
	    puts  "Event failed for N of $N."
	    puts  $Ffail "$timestep"
	    return 1
	}

	# Integrate (thus field only updates every 100 timesteps)
	puts $Fstate "$ncis $ncavi $ntrans"
	integrate 100
	incr timestep 100
	if { $vis_flag == 1 } {
	    imd positions
	}
    }

    return 0
}


##########################################################################
##########################################################################

# Actually run the program

# Output files (common to all events)
set CaseName "${N}_${reff_cavi}_${force_pore}_${nevents}_${rseed}"
set Fstate [open "data/${CaseName}_states.dat" "a+"]
set Ffail  [open "data/${CaseName}_fail.dat"   "a+"]

# Run nevents translocations
for { set casenum 1 } { $casenum <= $nevents } { incr casenum } {

    # Run case
    set errcode 1
    while { $errcode == 1 } {
	set errcode [RunMain $N $reff_cavi $force_pore $vis_flag $Fstate $Ffail]

	# Unbond the polymer (so the function can be called again)
	for { set i 1 } { $i < $N } { incr i } {
	    part $i bond delete
	}   

	# Delete the constraints
	constraint delete
    }

    # Print newline to Fstate to separate arrays
    # Ffail doesn't need this
    puts $Fstate ""
}

close $Fstate
close $Ffail

