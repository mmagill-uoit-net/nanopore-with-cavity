import numpy as np
import matplotlib.pyplot as plt
import matplotlib

# Fontsizes
matplotlib.rcParams['svg.fonttype'] = 'none'
matplotlib.rcParams['font.family'] = 'sans-serif'
fontsize_innerlabels=16
fontsize_axislabels=20
fontsize_ticks=20

# Load data
#A = np.loadtxt('avg_metrics.dat')
A = np.loadtxt('avg_metrics.rnom13.dat')

# Dictionary
ind_N=0
ind_r=1
ind_F=2
ind_mean_ttrans=3
ind_std_ttrans=4
ind_mean_tlastthread=5
ind_std_tlastthread=6
ind_mean_texit=7
ind_std_texit=8
ind_mean_nexit=9
ind_std_nexit=10
ind_mean_tfill=11
ind_std_tfill=12
ind_mean_tstuck=13
ind_std_tstuck=14
ind_mean_nstuck=15
ind_std_nstuck=16
ind_mean_ntail=17
ind_std_ntail=18
ind_failrate=19
ind_mean_tfail=20
ind_std_tfail=21







# Tmp soln: Plot the six plots


# axarr[0,0].loglog(A[:,ind_N],A[:,ind_mean_ttrans])
# axarr[1,0].loglog(A[:,ind_N],A[:,ind_mean_tstuck])
# axarr[2,0].loglog(A[:,ind_N],A[:,ind_mean_texit])

# axarr[0,1].semilogx(A[:,ind_N],100*A[:,ind_failrate])
# axarr[1,1].semilogx(A[:,ind_N],A[:,ind_mean_nstuck])
# axarr[2,1].semilogx(A[:,ind_N],A[:,ind_mean_nexit])






### Case Filters

Filter_RadOf4pt0_ForceOf0pt4 = np.where( (abs(A[:,ind_r]-4.0)<0.01) & (abs(A[:,ind_F]-0.4)<0.01) )
Filter_RadOf4pt0_ForceOf0pt5 = np.where( (abs(A[:,ind_r]-4.0)<0.01) & (abs(A[:,ind_F]-0.5)<0.01) )
Filter_RadOf4pt0_ForceOf0pt6 = np.where( (abs(A[:,ind_r]-4.0)<0.01) & (abs(A[:,ind_F]-0.6)<0.01) )
Filter_RadOf4pt0_ForceOf0pt75 = np.where( (abs(A[:,ind_r]-4.0)<0.01) & (abs(A[:,ind_F]-0.75)<0.01) )
Filter_RadOf4pt0_ForceOf1pt0 = np.where( (abs(A[:,ind_r]-4.0)<0.01) & (abs(A[:,ind_F]-1.0)<0.01) )
Filter_RadOf4pt0_ForceOf1pt25 = np.where( (abs(A[:,ind_r]-4.0)<0.01) & (abs(A[:,ind_F]-1.25)<0.01) )
Filter_RadOf4pt0_ForceOf1pt5 = np.where( (abs(A[:,ind_r]-4.0)<0.01) & (abs(A[:,ind_F]-1.5)<0.01) )
Filter_RadOf4pt0_ForceOf1pt6 = np.where( (abs(A[:,ind_r]-4.0)<0.01) & (abs(A[:,ind_F]-1.6)<0.01) )
Filter_RadOf4pt0_ForceOf1pt7 = np.where( (abs(A[:,ind_r]-4.0)<0.01) & (abs(A[:,ind_F]-1.7)<0.01) )
Filter_RadOf4pt0_ForceOf1pt8 = np.where( (abs(A[:,ind_r]-4.0)<0.01) & (abs(A[:,ind_F]-1.8)<0.01) )
Filter_RadOf4pt0_ForceOf1pt9 = np.where( (abs(A[:,ind_r]-4.0)<0.01) & (abs(A[:,ind_F]-1.9)<0.01) )
Filter_RadOf4pt0_ForceOf2pt0 = np.where( (abs(A[:,ind_r]-4.0)<0.01) & (abs(A[:,ind_F]-2.0)<0.01) )
Filter_RadOf4pt0_ForceOf3pt0 = np.where( (abs(A[:,ind_r]-4.0)<0.01) & (abs(A[:,ind_F]-3.0)<0.01) )
Filter_RadOf4pt0_ForceOf4pt0 = np.where( (abs(A[:,ind_r]-4.0)<0.01) & (abs(A[:,ind_F]-4.0)<0.01) )

Filter_RadOf3pt0_ForceOf0pt4 = np.where( (abs(A[:,ind_r]-3.0)<0.01) & (abs(A[:,ind_F]-0.4)<0.01) )
Filter_RadOf3pt0_ForceOf0pt5 = np.where( (abs(A[:,ind_r]-3.0)<0.01) & (abs(A[:,ind_F]-0.5)<0.01) )
Filter_RadOf3pt0_ForceOf0pt6 = np.where( (abs(A[:,ind_r]-3.0)<0.01) & (abs(A[:,ind_F]-0.6)<0.01) )
Filter_RadOf3pt0_ForceOf0pt75 = np.where( (abs(A[:,ind_r]-3.0)<0.01) & (abs(A[:,ind_F]-0.75)<0.01) )
Filter_RadOf3pt0_ForceOf1pt0 = np.where( (abs(A[:,ind_r]-3.0)<0.01) & (abs(A[:,ind_F]-1.0)<0.01) )
Filter_RadOf3pt0_ForceOf1pt25 = np.where( (abs(A[:,ind_r]-3.0)<0.01) & (abs(A[:,ind_F]-1.25)<0.01) )
Filter_RadOf3pt0_ForceOf1pt5 = np.where( (abs(A[:,ind_r]-3.0)<0.01) & (abs(A[:,ind_F]-1.5)<0.01) )

Filter_RadOf3pt5_ForceOf0pt4 = np.where( (abs(A[:,ind_r]-3.5)<0.01) & (abs(A[:,ind_F]-0.4)<0.01) )
Filter_RadOf3pt5_ForceOf0pt5 = np.where( (abs(A[:,ind_r]-3.5)<0.01) & (abs(A[:,ind_F]-0.5)<0.01) )
Filter_RadOf3pt5_ForceOf0pt6 = np.where( (abs(A[:,ind_r]-3.5)<0.01) & (abs(A[:,ind_F]-0.6)<0.01) )
Filter_RadOf3pt5_ForceOf0pt75 = np.where( (abs(A[:,ind_r]-3.5)<0.01) & (abs(A[:,ind_F]-0.75)<0.01) )
Filter_RadOf3pt5_ForceOf1pt0 = np.where( (abs(A[:,ind_r]-3.5)<0.01) & (abs(A[:,ind_F]-1.0)<0.01) )
Filter_RadOf3pt5_ForceOf1pt25 = np.where( (abs(A[:,ind_r]-3.5)<0.01) & (abs(A[:,ind_F]-1.25)<0.01) )
Filter_RadOf3pt5_ForceOf1pt5 = np.where( (abs(A[:,ind_r]-3.5)<0.01) & (abs(A[:,ind_F]-1.5)<0.01) )


### Function Definitions

## 
def PlotMany_ttrans(A,Filters):
        # Label positions
        LabelPosN = np.array([15.,11.,11.,11.,11.,11.,11.])
        LabelPosy = np.array([1.9e5,8.5e4,4.5e4,2.e4,8e3,3e3])
        LabelAngles = np.array([-13,-13,-12,-8,5,5])
	# Iterate over the filters
        for j in range(len(Filters)):
            Filter = Filters[j]
            B = A[Filter]
            last_plot = plt.loglog(B[:,ind_N],B[:,ind_mean_ttrans],'-*',lw=3)
#            last_plot = plt.plot(B[:,ind_N],B[:,ind_mean_ttrans],'-*',lw=3)
            # Labels
            Labely = LabelPosy[j]
            LabelN = LabelPosN[j]
            plt.text(LabelN,Labely,"F = %.2f"%B[0,ind_F],color=last_plot[0].get_color(),fontsize=fontsize_innerlabels,rotation=LabelAngles[j])
        # Arrows indicating force trends
        opt = dict(color='k',width=0.5)
        #plt.annotate('Increasing F',xytext=(60,3.e4),xy=(150,1.3e3),fontsize=fontsize_innerlabels,arrowprops=opt)
	# Set titles
        plt.xlabel('$N$',fontsize=fontsize_axislabels)
        plt.ylabel('$t_{\mathrm{trans}}$',fontsize=fontsize_axislabels)
	# Set limits
        plt.xlim([10,400])
        #plt.ylim([2e3,3e4])
        plt.ylim([2.e3,3e5])
        # Extra ticks
        extra_ticks = np.concatenate([np.arange(20,100,20),np.arange(100,400+1,100)],axis=1)
        #plt.gca().get_xaxis().set_ticks(extra_ticks)
        #plt.gca().get_xaxis().set_ticklabels(extra_ticks)
        matplotlib.rcParams['xtick.labelsize'] = fontsize_ticks
        matplotlib.rcParams['ytick.labelsize'] = fontsize_ticks
	# Display plot
	#plt.show()
# End of function #

## 
def PlotMany_tstuck(A,Filters):
	# Iterate over the filters
        for Filter in Filters:
		B = A[Filter]
                last_plot = plt.loglog(B[:,ind_N],B[:,ind_mean_tstuck],'-*',lw=3)
        # Arrows indicating force trends
        opt = dict(color='k',width=0.5)
        plt.annotate('Increasing F',xytext=(35,1.7e5),xy=(50,1e3),fontsize=fontsize_innerlabels,arrowprops=opt)
	# Set titles
        plt.xlabel('$N$',fontsize=fontsize_axislabels)
        plt.ylabel('$t_{\mathrm{stuck}}$',fontsize=fontsize_axislabels)
	# Set limits
        plt.xlim([10,400])
        plt.ylim([1e3,3e5])
        # Extra ticks
        matplotlib.rcParams['xtick.labelsize'] = fontsize_ticks
        matplotlib.rcParams['ytick.labelsize'] = fontsize_ticks
	# Display plot
	#plt.show()
# End of function #

## 
def PlotMany_texit(A,Filters):
    # Iterate over the filters
        for Filter in Filters:
		B = A[Filter]
                last_plot = plt.loglog(B[:,ind_N],B[:,ind_mean_texit],'-*',lw=3)
        # Arrows indicating force trends
        opt = dict(color='k',width=0.5)
        plt.annotate('Increasing F',xytext=(12,2e3),xy=(250,40),fontsize=fontsize_innerlabels,arrowprops=opt)
	# Set titles
        plt.xlabel('$N$',fontsize=fontsize_axislabels)
        plt.ylabel('$t_{\mathrm{exit}}$',fontsize=fontsize_axislabels)
	# Set limits
        plt.xlim([10,400])
        plt.ylim([2e1,2e4])
	# Scaling Law
	Ns = np.arange(10,400,1)
        SNprefac=3.71
        SNalpha=1.47
        plt.loglog(Ns,1.5*(5./3.)*(Ns)**(SNalpha),'k--')
        plt.text(50.,8.e3,r'$\tau \,=\, %.2f\, N^{\,%.2f}$'%(SNprefac,SNalpha),color='k',fontsize=fontsize_innerlabels)
        # Extra ticks
        extra_ticks = np.concatenate([np.arange(20,100,20),np.arange(100,400+1,100)],axis=1)
        #plt.gca().get_xaxis().set_ticks(extra_ticks)
        #plt.gca().get_xaxis().set_ticklabels(extra_ticks)
        matplotlib.rcParams['xtick.labelsize'] = fontsize_ticks
        matplotlib.rcParams['ytick.labelsize'] = fontsize_ticks
	# Display plot
	#plt.show()
# End of function #

## 
def PlotMany_failrate(A,Filters):
    # Iterate over the filters
        for Filter in Filters:
		B = A[Filter]
                last_plot = plt.semilogx(B[:,ind_N],100*B[:,ind_failrate],'-*',lw=3)
        # Arrows indicating force trends
        opt = dict(color='k',width=0.5)
        plt.annotate('Increasing F',xytext=(12,85),xy=(70,5),fontsize=fontsize_innerlabels,arrowprops=opt)
	# Set titles
        plt.xlabel('$N$',fontsize=fontsize_axislabels)
        plt.ylabel('$\mathrm{Failrate\,[\%]}$',fontsize=fontsize_axislabels)
	# Set limits
        plt.xlim([10,400])
        plt.ylim([0,100])
        # Extra ticks
        extra_ticks = np.concatenate([np.arange(20,100,20),np.arange(100,400+1,100)],axis=1)
        #plt.gca().get_xaxis().set_ticks(extra_ticks)
        #plt.gca().get_xaxis().set_ticklabels(extra_ticks)
        matplotlib.rcParams['xtick.labelsize'] = fontsize_ticks
        matplotlib.rcParams['ytick.labelsize'] = fontsize_ticks
	# Display plot
	#plt.show()
# End of function #

## 
def PlotMany_nstuck(A,Filters):
    # Iterate over the filters
        for Filter in Filters:
		B = A[Filter]
                last_plot = plt.semilogx(B[:,ind_N],B[:,ind_mean_nstuck],'-*',lw=3)
                Ncrit_ttrans = B[np.argmin(B[:,ind_mean_ttrans]),ind_N]
                Ncrit_tstuck = B[np.argmin(B[:,ind_mean_tstuck]),ind_N]
                Ncrit_nstuck = B[np.argmax(B[:,ind_mean_nstuck]),ind_N]
                print "%.1f %.1f %d %d %d" % (B[0,ind_r],B[0,ind_F],Ncrit_ttrans,Ncrit_tstuck,Ncrit_nstuck)
        # Arrows indicating force trends
        opt = dict(color='k',width=0.5)
        plt.annotate('Increasing F',xytext=(35,20),xy=(350,155),fontsize=fontsize_innerlabels,arrowprops=opt)
	# Set titles
        plt.xlabel('$N$',fontsize=fontsize_axislabels)
        plt.ylabel('$N_{\mathrm{stuck}}$',fontsize=fontsize_axislabels)
	# Set limits
        plt.xlim([10,400])
        plt.ylim([10,160])
        # Scaling Law
	Ns = np.arange(10,400,1)
        plt.semilogx(Ns,Ns,'k--')
        plt.text(25.,85.,"$N_{\mathrm{stuck}} = N$",
                        color='k',fontsize=fontsize_innerlabels)
        # Extra ticks
        extra_yticks = np.array([10,40,70,100,130,160])
        plt.gca().get_yaxis().set_ticks(extra_yticks)
        plt.gca().get_yaxis().set_ticklabels(extra_yticks)
        matplotlib.rcParams['xtick.labelsize'] = fontsize_ticks
        matplotlib.rcParams['ytick.labelsize'] = fontsize_ticks
	# Display plot
	#plt.show()
# End of function #

## 
def PlotMany_nexit(A,Filters):
    # Iterate over the filters
        for Filter in Filters:
		B = A[Filter]
                last_plot = plt.semilogx(B[:,ind_N],B[:,ind_mean_nexit],'-*',lw=3)
        # Arrows indicating force trends
        opt = dict(color='k',width=0.5)
        plt.annotate('Increasing F',xytext=(35,10),xy=(300,70),fontsize=fontsize_innerlabels,arrowprops=opt)
	# Set titles
        plt.xlabel('$N$',fontsize=fontsize_axislabels)
        plt.ylabel('$N_{\mathrm{exit}}$',fontsize=fontsize_axislabels)
	# Set limits
        plt.xlim([10,400])
        plt.ylim([5,75])
        # Scaling Law
	Ns = np.arange(10,400,1)
        plt.semilogx(Ns,Ns/2,'k--')
        plt.text(20.,40.,"$N_{\mathrm{exit}} = N/2$",
                        color='k',fontsize=fontsize_innerlabels)
        # Extra ticks
        extra_yticks = np.array([10,25,40,55,70])
        plt.gca().get_yaxis().set_ticks(extra_yticks)
        plt.gca().get_yaxis().set_ticklabels(extra_yticks)
        matplotlib.rcParams['xtick.labelsize'] = fontsize_ticks
        matplotlib.rcParams['ytick.labelsize'] = fontsize_ticks
	# Display plot
	#plt.show()
# End of function #

## 
def PlotMany_ntail(A,Filters):
    # Iterate over the filters
        for Filter in Filters:
		B = A[Filter]
                last_plot = plt.semilogx(B[:,ind_N],B[:,ind_mean_ntail],'-*',lw=3)
        # Arrows indicating force trends
        opt = dict(color='k',width=0.5)
        plt.annotate('Increasing F',xytext=(50,100),xy=(175,50),fontsize=fontsize_innerlabels,arrowprops=opt)
	# Set titles
        plt.xlabel('$N$',fontsize=fontsize_axislabels)
        plt.ylabel('$N_{\mathrm{tail}}$',fontsize=fontsize_axislabels)
	# Set limits
        plt.xlim([10,400])
        plt.ylim([0,400])
        # Scaling Law
	Ns = np.arange(10,400,1)
        plt.semilogx(Ns,Ns/2,'k--')
        plt.text(35.,35.,"$N_{\mathrm{exit}} = N/2$",
                        color='k',fontsize=fontsize_innerlabels)
        # Extra ticks
        extra_ticks = np.concatenate([np.arange(20,100,20),np.arange(100,400+1,100)],axis=1)
        #plt.gca().get_xaxis().set_ticks(extra_ticks)
        #plt.gca().get_xaxis().set_ticklabels(extra_ticks)
        matplotlib.rcParams['xtick.labelsize'] = fontsize_ticks
        matplotlib.rcParams['ytick.labelsize'] = fontsize_ticks
	# Display plot
	#plt.show()
# End of function #

## 
def PlotMany_NcritComp(A,Filters):
    # Iterate over the filters
        color_by_radius = ['r','g','b']
        marker_by_radius = ['*','^','o']
        for Filter in Filters:
		B = A[Filter]
                Ncrit_Nstuck = B[np.argmax(B[:,ind_mean_nstuck]),ind_N]
                Ncrit_Ttrans = B[np.argmin(B[:,ind_mean_ttrans]),ind_N]
                if (B[1,ind_F]==0.4):
                        plt.plot(Ncrit_Nstuck,Ncrit_Ttrans,'*',markersize=15,
                                 marker=marker_by_radius[int(round(B[0,ind_r]/0.5))-6],
                                 markerfacecolor='none',
                                 markeredgecolor=color_by_radius[int(round(B[0,ind_r]/0.5))-6],
                                 markeredgewidth=3,
                                 label='$r_{\mathrm{eff}}$ = %.1f'%B[1,ind_r])
                else:
                        plt.plot(Ncrit_Nstuck,Ncrit_Ttrans,'*',markersize=15,
                                 marker=marker_by_radius[int(round(B[0,ind_r]/0.5))-6],
                                 markerfacecolor='none',
                                 markeredgecolor=color_by_radius[int(round(B[0,ind_r]/0.5))-6],
                                 markeredgewidth=3)
        # Arrows indicating force trends
        opt = dict(color='k',width=0.5)
        plt.annotate('Increasing F',xytext=(20,10),xy=(120,80),fontsize=fontsize_innerlabels,arrowprops=opt)
	# Set titles
        plt.xlabel('$N_{\mathrm{stuck}}^*$',fontsize=fontsize_axislabels)
        plt.ylabel('$N_{\mathrm{trans}}^*$',fontsize=fontsize_axislabels)
	# Set limits
        plt.xlim([0,150])
        plt.ylim([0,150])
        # Scaling Law
	Ns = np.arange(0,200,1)
        plt.plot(Ns,Ns,'k--',lw=3)
	# Display plot
        plt.legend(numpoints=1,loc='upper left')
	plt.show()
# End of function #

##
def PlotSixPanel(A,Filters):
    f, axarr = plt.subplots(3,2)
    plt.sca(axarr[0,0])
    PlotMany_ttrans(A,Filters)
    plt.text(250,1.5e5,'(a)',fontsize=fontsize_innerlabels)
    plt.text(850,1.5e5,'(d)',fontsize=fontsize_innerlabels)
    plt.xlabel('')
    plt.gca().get_xaxis().set_ticklabels([])
    plt.sca(axarr[1,0])
    PlotMany_tstuck(A,Filters)
    plt.text(250,1.5e5,'(b)',fontsize=fontsize_innerlabels)
    plt.xlabel('')
    plt.gca().get_xaxis().set_ticklabels([])
    plt.sca(axarr[2,0])
    PlotMany_texit(A,Filters)
    plt.text(12,8e3,'(c)',fontsize=fontsize_innerlabels)
    plt.xlabel('')
    plt.text(50,6.5,'$N$',fontsize=fontsize_axislabels)

    plt.sca(axarr[0,1])
    plt.axis('off')
    plt.sca(axarr[1,1])
    PlotMany_nstuck(A,Filters)
    plt.text(12,142,'(e)',fontsize=fontsize_innerlabels)
    plt.xlabel('')
    plt.gca().get_xaxis().set_ticklabels([])
    plt.sca(axarr[2,1])
    PlotMany_nexit(A,Filters)
    plt.text(12,66,'(f)',fontsize=fontsize_innerlabels)
    plt.xlabel('')
    plt.text(50,-3,'$N$',fontsize=fontsize_axislabels)

    plt.subplots_adjust(left=0.10, bottom=0.07, right=0.98, top=0.98,
                        wspace=0.25, hspace=0.08)
    plt.show()
# End of function #


### Interactive

PlotSixPanel(A,[Filter_RadOf4pt0_ForceOf0pt4,
Filter_RadOf4pt0_ForceOf0pt5,
#Filter_RadOf4pt0_ForceOf0pt6,
Filter_RadOf4pt0_ForceOf0pt75,
Filter_RadOf4pt0_ForceOf1pt0,
#Filter_RadOf4pt0_ForceOf1pt25,
#Filter_RadOf4pt0_ForceOf1pt5,
#Filter_RadOf4pt0_ForceOf1pt6,
#Filter_RadOf4pt0_ForceOf1pt7,
#Filter_RadOf4pt0_ForceOf1pt8,
#Filter_RadOf4pt0_ForceOf1pt9,
Filter_RadOf4pt0_ForceOf2pt0,
Filter_RadOf4pt0_ForceOf3pt0,
#Filter_RadOf4pt0_ForceOf4pt0
])

# PlotSixPanel(A,[Filter_RadOf3pt0_ForceOf0pt4,
# Filter_RadOf3pt0_ForceOf0pt5,
# Filter_RadOf3pt0_ForceOf0pt6,
# Filter_RadOf3pt0_ForceOf0pt75,
# Filter_RadOf3pt0_ForceOf1pt0,
# Filter_RadOf3pt0_ForceOf1pt5
# ])

# PlotSixPanel(A,[Filter_RadOf3pt5_ForceOf0pt4,
# Filter_RadOf3pt5_ForceOf0pt5,
# Filter_RadOf3pt5_ForceOf0pt6,
# Filter_RadOf3pt5_ForceOf0pt75,
# Filter_RadOf3pt5_ForceOf1pt0,
# Filter_RadOf3pt5_ForceOf1pt5
# ])

# PlotMany_failrate(A,[Filter_RadOf4pt0_ForceOf0pt4,
#                      Filter_RadOf4pt0_ForceOf0pt5,
#                      Filter_RadOf4pt0_ForceOf0pt75,
#                      Filter_RadOf4pt0_ForceOf1pt0,
#                      Filter_RadOf4pt0_ForceOf3pt0])

# PlotMany_NcritComp(A,[
# Filter_RadOf3pt0_ForceOf0pt4,
# Filter_RadOf3pt0_ForceOf0pt5,
# Filter_RadOf3pt0_ForceOf0pt6,
# Filter_RadOf3pt0_ForceOf0pt75,
# Filter_RadOf3pt0_ForceOf1pt0,
# Filter_RadOf3pt5_ForceOf0pt4,
# Filter_RadOf3pt5_ForceOf0pt5,
# Filter_RadOf3pt5_ForceOf0pt6,
# Filter_RadOf3pt5_ForceOf0pt75,
# Filter_RadOf3pt5_ForceOf1pt0,
# Filter_RadOf4pt0_ForceOf0pt4,
# Filter_RadOf4pt0_ForceOf0pt5,
# Filter_RadOf4pt0_ForceOf0pt6,
# Filter_RadOf4pt0_ForceOf0pt75,
# Filter_RadOf4pt0_ForceOf1pt0
# ])
