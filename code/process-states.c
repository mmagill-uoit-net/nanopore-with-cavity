#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>


// Reads in output for a case from the data folder
// Each run has a pair of files, one _states and one _fail
// This file processes the _states files

// Program Inputs
// Takes in the _states.dat filename of the case.

// Case Inputs
// Example filename: 350_3._1.0_100_3501123456_states.dat
// N = 350
// reffcavi = 3.0
// Fpore = 1.0
// nevents = 100
// rseed = 3501123456

// Case Outputs
// t_trans: 	total translocation time
// t_lastthread:the last time ntrans was 0
// t_exit: 	the time after last thread
// n_exit: 	<ncavi>_{t > t_lastthread}
// n_thresh: 	set to min(N,n(R_G==reffcavi))
// t_fill: 	first time ncavi>=N_thresh
// t_stuck:	t_lastthread - t_fill
// n_stuck:	<ncavi>_{t_fill < t < t_lastthread}
// n_tail:     	<ncis>_{t_fill < t < t_lastthread}


int main (int argc, char* argv[]) {

  // Calculation parameters
  float threshold_factor = 0.5;

  // Parse input: file should be of the form data/[...].dat
  if ( argc != 2 ) { printf("%s data/[_states filename]\n",argv[0]); return 1; }
  char* filename_states = argv[1]; // Copy this for later
  int N; float reffcavi; float Fpore; int nevents; long int rseed;
  sscanf(filename_states,"data/%d_%f_%f_%d_%ld_states.dat",&N,&reffcavi,&Fpore,&nevents,&rseed);
  //printf("%d %f %f %d %ld\n",N,reffcavi,Fpore,nevents,rseed);

  // Input files
  FILE* fstates = fopen(filename_states,"r"); //printf("%s\n",filename_states); 

  // Output files
  char* filename_output = "metrics_states.dat"; FILE* fout_states = fopen(filename_output,"a");

  /////////////////////////////////////////////////////////////////////////////////////////////////

  //// MAIN LOOP

  char line[256];
  int ncis; int ncavi; int ntrans;
  int ttrans=0; 
  int tlastthread; int texit;
  float nexit; int nexitSum=0; 
  // Use R_G = N^(0.588) / sqrt(6) := reffcavi
  float nthresh = pow( reffcavi * pow(6.,0.5) , 1./0.588); nthresh = fmin(nthresh,(float)N);
  int tfill=0; int tstuck;
  float nstuck; int nstuckSum=0; int nstuckSum_excess=0;
  float ntail; int ntailSum=0; int ntailSum_excess=0;

  // Loop over lines in states file
  while (fgets(line,256,fstates)) {

    // Blank line means end of the event
    if (line[0] == '\n') {

      // Finish metric calculations
      texit = ttrans - tlastthread;
      nexit = (float) nexitSum / texit;
      tstuck = tlastthread - tfill;
      nstuck = (float) (nstuckSum - nstuckSum_excess) / tstuck;
      ntail = (float) (ntailSum - ntailSum_excess) / tstuck;

      // Output metrics
      fprintf(fout_states,"%d %f %f %d %ld %d %d %d %f %f %d %d %f %f\n",
	      N,reffcavi,Fpore,nevents,rseed,
	      ttrans,tlastthread,texit,nexit,nthresh,tfill,tstuck,nstuck,ntail);

      // Reset metric trackers
      ttrans=0; nexitSum=0; tfill=0; nstuckSum=0; nstuckSum_excess=0; ntailSum=0; ntailSum_excess=0;

    }

    // Otherwise continue processing the current event
    else {

      // Scan next line for the event
      sscanf(line,"%d %d %d\n",&ncis,&ncavi,&ntrans);

      //// FOR ALL T
      ttrans++;

      //// FOR T = T_FILL
      // Identify the first time ncavi > nthresh
      // After first time, this loop isn't entered
      if ((tfill==0)&&(ncavi>=nthresh)) {tfill=ttrans;}

      //// FOR T > T_FILL
      // Sum over t > t_fill
      // The t > t_lastthread sum is also computed, later subtracted
      if (tfill!=0) {
	nstuckSum+=ncavi;
	ntailSum+=ncis;
      }

      //// FOR T >= T_LASTTHREAD
      // This loop runs occasionally until t > t_lastthread
      // So the final run occurs at t = t_lastthread
      if (ntrans==0) {
	// Last time this is overridden is the correct value
	tlastthread=ttrans;

	// Reset these sums until t > t_lastthread
	nexitSum=0;
	nstuckSum_excess=0;
	ntailSum_excess=0;
      }
      // Thus these sum over t > t_lastthread
      nexitSum+=ncavi;
      nstuckSum_excess+=ncavi;
      ntailSum_excess+=ncis;

    }
  }


  fclose(fout_states);
  fclose(fstates);
  return 0;
}
