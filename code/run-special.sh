#!/bin/bash


# Run rnom13 at high forces
# Reduced number of cases for faster turnaround

# Constant parameters
rseed_suffix=123456
vis_flag=0
nevents=10
reff_cavi=3.5
force_pore=0.4

# Make log directory if it isn't already there
mkdir -p logs

# Should tailor time limit to each set of cases... but not for now
time=1000m

for rseed_prefix in {201..400}
do

   for N in 47 48
   do
      rseed=${N}${rseed_prefix}${rseed_suffix}
      label=${reff_cavi}_${force_pore}_${N}_${rseed}
      sqsub -q serial -o logs/log_${label}.log -r $time ~/espresso-2.1.2j/Espresso  nanopore-with-cavity-rnom13.tcl $N $reff_cavi $force_pore $vis_flag $nevents $rseed
   done

done


