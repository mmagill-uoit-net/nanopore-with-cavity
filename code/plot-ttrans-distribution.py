import numpy as np
import matplotlib.pyplot as plt


# Take the metrics_states and metrics_fails files
#  and compute final averages for each (N,r,F)

Astates = np.loadtxt('metrics_states.dat')
#Afails = np.loadtxt('metrics_fails.dat')

### Need to remove straight-through events
### Maybe make this a variable as a function of (N,r,F)

# Identify (N,r,F) list
rs = np.unique(Astates[:,1])
Fs = np.unique(Astates[:,2])
Output = np.zeros((0,3+19))


# Find indices of rows corresponding to (N,r,F) cases
def Filter_partial(A,r,F):
    return np.where( ( abs(A[:,1]-r)<0.01 ) & 
                     ( abs(A[:,2]-F)<0.01 ) )

def Filter(A,N,r,F):
    return np.where( ( abs(A[:,0]-N)<0.01 ) & 
                     ( abs(A[:,1]-r)<0.01 ) & 
                     ( abs(A[:,2]-F)<0.01 ) )




# Histogram

B = Astates[Filter(Astates,50,4.0,0.5)]

hist, bins = np.histogram(np.log(B[:,5]), bins=50)
width = 0.7 * (bins[1] - bins[0])
center = (bins[:-1] + bins[1:]) / 2
plt.bar(center, hist, align='center', width=width)
plt.show()

