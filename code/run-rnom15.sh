#!/bin/bash


# Same as run.sh, but seed prefixes from 21 to 25 instead
# Also includes the F=0.6 and F=1.25 cases


# Constant parameters
rseed_suffix=123456
reff_cis=1.
vis_flag=0
nevents=100

# Make log directory if it isn't already theree
mkdir -p logs

# Should tailor time limit to each set of cases... but not for now
time=10000m

for rseed_prefix in {11..15} {21..25}
do

   ############################ Small cavity
   reff_cavi=3.

   force_pore=0.4
   for N in 10 15 20 25 30 50 100 150 200
   do
      rseed=${N}${rseed_prefix}${rseed_suffix}
      label=${reff_cavi}_${force_pore}_${N}_${rseed}
      sqsub -q serial -o logs/log_${label}.log -r $time ~/espresso-2.1.2j/Espresso nanopore-with-cavity.tcl $N $reff_cavi $force_pore $vis_flag $nevents $rseed
   done

   force_pore=0.5
   for N in 10 20 50 100 150 200 15 17 19 21 23 25 40 28 31 34 37
   do
      rseed=${N}${rseed_prefix}${rseed_suffix}
      label=${reff_cavi}_${force_pore}_${N}_${rseed}
      sqsub -q serial -o logs/log_${label}.log -r $time ~/espresso-2.1.2j/Espresso nanopore-with-cavity.tcl $N $reff_cavi $force_pore $vis_flag $nevents $rseed
   done

   force_pore=0.6
   for N in {30..40}
   do
      rseed=${N}${rseed_prefix}${rseed_suffix}
      label=${reff_cavi}_${force_pore}_${N}_${rseed}
      sqsub -q serial -o logs/log_${label}.log -r $time ~/espresso-2.1.2j/Espresso nanopore-with-cavity.tcl $N $reff_cavi $force_pore $vis_flag $nevents $rseed
   done

   force_pore=0.75
   for N in 300 400 10 15 20 25 30 35 40 50 100 150 200 
   do
      rseed=${N}${rseed_prefix}${rseed_suffix}
      label=${reff_cavi}_${force_pore}_${N}_${rseed}
      sqsub -q serial -o logs/log_${label}.log -r $time ~/espresso-2.1.2j/Espresso nanopore-with-cavity.tcl $N $reff_cavi $force_pore $vis_flag $nevents $rseed
   done

   force_pore=1.0
   for N in 250 300 350 400 10 15 20 100 150 200 25 35 45 55 65 75 38 41 44 47 50
   do
      rseed=${N}${rseed_prefix}${rseed_suffix}
      label=${reff_cavi}_${force_pore}_${N}_${rseed}
      sqsub -q serial -o logs/log_${label}.log -r $time ~/espresso-2.1.2j/Espresso nanopore-with-cavity.tcl $N $reff_cavi $force_pore $vis_flag $nevents $rseed
   done

   force_pore=1.25
   for N in {50..60}
   do
      rseed=${N}${rseed_prefix}${rseed_suffix}
      label=${reff_cavi}_${force_pore}_${N}_${rseed}
      sqsub -q serial -o logs/log_${label}.log -r $time ~/espresso-2.1.2j/Espresso nanopore-with-cavity.tcl $N $reff_cavi $force_pore $vis_flag $nevents $rseed
   done

   force_pore=1.5
   for N in 240 280 320 360 400 10 20 30 40 50 55 60 65 70 75 100 150 200
   do
      rseed=${N}${rseed_prefix}${rseed_suffix}
      label=${reff_cavi}_${force_pore}_${N}_${rseed}
      sqsub -q serial -o logs/log_${label}.log -r $time ~/espresso-2.1.2j/Espresso nanopore-with-cavity.tcl $N $reff_cavi $force_pore $vis_flag $nevents $rseed
   done




   ############################ Medium Cavity
   reff_cavi=3.5

   force_pore=0.4
   for N in 10 20 30 35 40 45 50 100 150 200
   do
      rseed=${N}${rseed_prefix}${rseed_suffix}
      label=${reff_cavi}_${force_pore}_${N}_${rseed}
      sqsub -q serial -o logs/log_${label}.log -r $time ~/espresso-2.1.2j/Espresso nanopore-with-cavity.tcl $N $reff_cavi $force_pore $vis_flag $nevents $rseed
   done

   force_pore=0.5
   for N in 10 20 30 100 150 200 35 40 45 48 50 55 60
   do
      rseed=${N}${rseed_prefix}${rseed_suffix}
      label=${reff_cavi}_${force_pore}_${N}_${rseed}
      sqsub -q serial -o logs/log_${label}.log -r $time ~/espresso-2.1.2j/Espresso nanopore-with-cavity.tcl $N $reff_cavi $force_pore $vis_flag $nevents $rseed
   done

   force_pore=0.6
   for N in {50..70..2}
   do
      rseed=${N}${rseed_prefix}${rseed_suffix}
      label=${reff_cavi}_${force_pore}_${N}_${rseed}
      sqsub -q serial -o logs/log_${label}.log -r $time ~/espresso-2.1.2j/Espresso nanopore-with-cavity.tcl $N $reff_cavi $force_pore $vis_flag $nevents $rseed
   done

   force_pore=0.75
   for N in 300 400 10 20 30 40 50 55 60 65 70 75 100 150 200
   do
      rseed=${N}${rseed_prefix}${rseed_suffix}
      label=${reff_cavi}_${force_pore}_${N}_${rseed}
      sqsub -q serial -o logs/log_${label}.log -r $time ~/espresso-2.1.2j/Espresso nanopore-with-cavity.tcl $N $reff_cavi $force_pore $vis_flag $nevents $rseed
   done

   force_pore=1.0
   for N in 300 400 10 20 40 60 125 150 200 65 70 75 80 85 90 95
   do
      rseed=${N}${rseed_prefix}${rseed_suffix}
      label=${reff_cavi}_${force_pore}_${N}_${rseed}
      sqsub -q serial -o logs/log_${label}.log -r $time ~/espresso-2.1.2j/Espresso nanopore-with-cavity.tcl $N $reff_cavi $force_pore $vis_flag $nevents $rseed
   done

   force_pore=1.25
   for N in {80..100..2}
   do
      rseed=${N}${rseed_prefix}${rseed_suffix}
      label=${reff_cavi}_${force_pore}_${N}_${rseed}
      sqsub -q serial -o logs/log_${label}.log -r $time ~/espresso-2.1.2j/Espresso nanopore-with-cavity.tcl $N $reff_cavi $force_pore $vis_flag $nevents $rseed
   done

   force_pore=1.5
   for N in 300 400 10 30 50 70 80 85 90 95 100 105 110 115 120 125 150 200
   do
      rseed=${N}${rseed_prefix}${rseed_suffix}
      label=${reff_cavi}_${force_pore}_${N}_${rseed}
      sqsub -q serial -o logs/log_${label}.log -r $time ~/espresso-2.1.2j/Espresso nanopore-with-cavity.tcl $N $reff_cavi $force_pore $vis_flag $nevents $rseed
   done




   ############################ Large Cavity
   reff_cavi=4.

   force_pore=0.4
   for N in 10 30 40 45 50 55 60 65 70 75 100 150 200
   do
      rseed=${N}${rseed_prefix}${rseed_suffix}
      label=${reff_cavi}_${force_pore}_${N}_${rseed}
      sqsub -q serial -o logs/log_${label}.log -r $time ~/espresso-2.1.2j/Espresso nanopore-with-cavity.tcl $N $reff_cavi $force_pore $vis_flag $nevents $rseed
   done

   force_pore=0.5
   for N in 10 20 50 100 150 200 30 40 45 55 60 80 65 70 75 85 90 95
   do
      rseed=${N}${rseed_prefix}${rseed_suffix}
      label=${reff_cavi}_${force_pore}_${N}_${rseed}
      sqsub -q serial -o logs/log_${label}.log -r $time ~/espresso-2.1.2j/Espresso nanopore-with-cavity.tcl $N $reff_cavi $force_pore $vis_flag $nevents $rseed
   done

   force_pore=0.6
   for N in {75..105..3}
   do
      rseed=${N}${rseed_prefix}${rseed_suffix}
      label=${reff_cavi}_${force_pore}_${N}_${rseed}
      sqsub -q serial -o logs/log_${label}.log -r $time ~/espresso-2.1.2j/Espresso nanopore-with-cavity.tcl $N $reff_cavi $force_pore $vis_flag $nevents $rseed
   done

   force_pore=0.75
   for N in 300 400 10 30 50 55 60 65 70 75 80 85 90 95 100 105 110 125 150 200
   do
      rseed=${N}${rseed_prefix}${rseed_suffix}
      label=${reff_cavi}_${force_pore}_${N}_${rseed}
      sqsub -q serial -o logs/log_${label}.log -r $time ~/espresso-2.1.2j/Espresso nanopore-with-cavity.tcl $N $reff_cavi $force_pore $vis_flag $nevents $rseed
   done

   force_pore=1.0
   for N in 250 300 350 400 10 20 50 100 150 200 75 85 95 105 115 125
   do
      rseed=${N}${rseed_prefix}${rseed_suffix}
      label=${reff_cavi}_${force_pore}_${N}_${rseed}
      sqsub -q serial -o logs/log_${label}.log -r $time ~/espresso-2.1.2j/Espresso nanopore-with-cavity.tcl $N $reff_cavi $force_pore $vis_flag $nevents $rseed
   done

   force_pore=1.25
   for N in {115..145..3}
   do
      rseed=${N}${rseed_prefix}${rseed_suffix}
      label=${reff_cavi}_${force_pore}_${N}_${rseed}
      sqsub -q serial -o logs/log_${label}.log -r $time ~/espresso-2.1.2j/Espresso nanopore-with-cavity.tcl $N $reff_cavi $force_pore $vis_flag $nevents $rseed
   done

   force_pore=1.5
   for N in 240 280 320 360 400 10 30 50 70 75 80 85 90 95 100 110 120 130 140 160 180 200
   do
      rseed=${N}${rseed_prefix}${rseed_suffix}
      label=${reff_cavi}_${force_pore}_${N}_${rseed}
      sqsub -q serial -o logs/log_${label}.log -r $time ~/espresso-2.1.2j/Espresso nanopore-with-cavity.tcl $N $reff_cavi $force_pore $vis_flag $nevents $rseed
   done


done






