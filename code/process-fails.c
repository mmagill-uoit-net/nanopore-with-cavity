#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>


// Reads in output for a case from the data folder
// Each run has a pair of files, one _states and one _fail
// This file processes the _fail files

// Program Inputs
// Takes in the _fail.dat filename of the case.

// Case Inputs
// Example filename: 350_3._1.0_100_3501123456_fail.dat
// N = 350
// reffcavi = 3.0
// Fpore = 1.0
// nevents = 100
// rseed = 3501123456

// Case Outputs
// n_fail:     	Number of failed events
// t_fail:     	Average time to fail for failed events


int main (int argc, char* argv[]) {

  // Parse input: file should be of the form data/[...].dat
  if ( argc != 2 ) { printf("%s data/[_fail filename]\n",argv[0]); return 1; }
  char* filename_fail = argv[1]; // Copy this for later
  int N; float reffcavi; float Fpore; int nevents; long int rseed;
  sscanf(filename_fail,"data/%d_%f_%f_%d_%ld_fail.dat",&N,&reffcavi,&Fpore,&nevents,&rseed);
  //printf("%d %f %f %d %ld\n",N,reffcavi,Fpore,nevents,rseed);

  // Input files
  FILE* ffail = fopen(filename_fail,"r"); //printf("%s\n",filename_fail); 

  // Output files
  char* filename_output = "metrics_fails.dat"; FILE* fout_fail = fopen(filename_output,"a");

  /////////////////////////////////////////////////////////////////////////////////////////////////

  //// MAIN LOOP

  char line[256];
  int failtime; // Tmp variable for fail time of each failure
  int nfail=0; // Total number of fails
  float tfail=0; // Average fail time

  // Loop over lines in states file
  while (fgets(line,256,ffail)) {

    // Scan next line for the event
    sscanf(line,"%d\n",&failtime);
    nfail++;
    tfail = tfail + failtime;
  }

  // Finish metric calculations
  if ( nfail > 0 ) { tfail = tfail / (float) nfail; }
  else { tfail = 0; }
  // Output metrics
  fprintf(fout_fail,"%d %f %f %d %ld %d %f\n",
	  N,reffcavi,Fpore,nevents,rseed,
	  nfail,tfail);


  fclose(fout_fail);
  fclose(ffail);
  return 0;
}
