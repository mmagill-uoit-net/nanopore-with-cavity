import numpy as np



# Take the metrics_states and metrics_fails files
#  and compute final averages for each (N,r,F)

Astates = np.loadtxt('metrics_states.dat')
Afails = np.loadtxt('metrics_fails.dat')

### Need to remove straight-through events
### Maybe make this a variable as a function of (N,r,F)

# Identify (N,r,F) list
rs = np.unique(Astates[:,1])
Fs = np.unique(Astates[:,2])
Output = np.zeros((0,3+19))


# Find indices of rows corresponding to (N,r,F) cases
def Filter_partial(A,r,F):
    return np.where( ( abs(A[:,1]-r)<0.01 ) & 
                     ( abs(A[:,2]-F)<0.01 ) )

def Filter(A,N,r,F):
    return np.where( ( abs(A[:,0]-N)<0.01 ) & 
                     ( abs(A[:,1]-r)<0.01 ) & 
                     ( abs(A[:,2]-F)<0.01 ) )


i=0
# Ns is not the same for every (r,F)
for r in rs:
    for F in Fs:
        Ns = np.unique(Astates[Filter_partial(Astates,r,F),0])
        for N in Ns:

            # Select the current (N,r,F)
            Bstates = Astates[Filter(Astates,N,r,F)]
            Bfails  =  Afails[Filter(Afails,N,r,F)]

            ## Filter out straight-through (ST) events
            Bstates_ST = Astates[np.where((Bstates[:,10]<=0)|
                                          (Bstates[:,11]<=0))]
            STrate = float(np.shape(Bstates_ST)[0]) / float(np.shape(Astates)[0])
            print "%.3f %%" % (100*STrate)
            Bstates = Bstates[np.where((Bstates[:,10]>0)&
                                       (Bstates[:,11]>0))]

            if Bstates.size > 0:
                ## Means
                # Translocation time
                mean_ttrans = np.mean(Bstates[:,5])
                # Last Thread time
                mean_tlastthread = np.mean(Bstates[:,6])
                # Exit time and count
                mean_texit = np.mean(Bstates[:,7])
                mean_nexit = np.mean(Bstates[:,8])
                # Fill time
                mean_tfill = np.mean(Bstates[:,10])
                # Stuck time and count, tail count
                mean_tstuck = np.mean(Bstates[:,11])
                mean_nstuck = np.average(Bstates[:,12],weights=Bstates[:,11])
                mean_ntail = np.average(Bstates[:,13],weights=Bstates[:,11])
                # Fail rate
                Nsuccesses = np.shape(Bstates)[0]
                Nfails = np.sum(Bfails[:,5])
                failrate = Nfails / (Nsuccesses + Nfails)
                # Average time to fail for failed events (already partially averaged)
                #mean_tfail = np.average(Bfails[:,6],weights=Bfails[:,5]) # Doesn't work when weights are all zero
                mean_tfail = np.average(Bfails[:,5]*Bfails[:,6])

                ## Standard Deviations
                # Translocation time
                std_ttrans = np.std(Bstates[:,5])
                # Last Thread time
                std_tlastthread = np.std(Bstates[:,6])
                # Exit time and count
                std_texit = np.std(Bstates[:,7])
                std_nexit = np.std(Bstates[:,8])
                # Fill time
                std_tfill = np.std(Bstates[:,10])
                # Stuck time and count, tail count
                std_tstuck = np.std(Bstates[:,11])
                var_nstuck = np.average((Bstates[:,12]-mean_nstuck)**2,weights=Bstates[:,11])
                var_ntail = np.average((Bstates[:,13]-mean_ntail)**2,weights=Bstates[:,11])
                std_nstuck = np.sqrt(var_nstuck)
                std_ntail = np.sqrt(var_ntail)
                # Average time to fail for failed events (already partially averaged)
                var_tfail = np.average((Bfails[:,6]-mean_tfail)**2,weights=Bfails[:,3])
                std_tfail = np.sqrt(var_tfail)
                
                # Save to matrix
                Output = np.vstack([Output,
                                    [N,r,F,
                                     mean_ttrans,std_ttrans,
                                     mean_tlastthread,std_tlastthread,
                                     mean_texit,std_texit,
                                     mean_nexit,std_nexit,
                                     mean_tfill,std_tfill,
                                     mean_tstuck,std_tstuck,
                                     mean_nstuck,std_nstuck,
                                     mean_ntail,std_ntail,
                                     failrate,
                                     mean_tfail,std_tfail]])
                i=i+1


        

np.savetxt('avg_metrics.dat',Output)



