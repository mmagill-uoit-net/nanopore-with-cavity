

with open('failedjobs.txt') as fin:
    data = fin.readlines()

for line in data:

    line_arr = line.split('.log')[0].split('_')

    reff_cavi = line_arr[1]
    force_pore = line_arr[2]
    N = line_arr[3]
    rseed = line_arr[4]

    label = reff_cavi + '_' + force_pore + '_' + N + '_' + rseed

    time = '10000m'
    vis_flag = '0'
    nevents = '100'
    print  "sqsub -q serial -o logs/log_%s.log -r %s ~/espresso-2.1.2j/Espresso nanopore-with-cavity-rnom13.tcl %s %s %s %s %s %s" % (label,time,N,reff_cavi,force_pore,vis_flag,nevents,rseed)

