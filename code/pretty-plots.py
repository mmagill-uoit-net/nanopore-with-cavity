import numpy as np
import matplotlib.pyplot as plt
import matplotlib

# Fontsizes
matplotlib.rcParams['svg.fonttype'] = 'none'
matplotlib.rcParams['font.family'] = 'sans-serif'
fontsize_innerlabels=20
fontsize_axislabels=26
fontsize_ticks=20

# Load data
A = np.loadtxt('avg_metrics.rnom13.dat')
B = np.loadtxt('avg_metrics.dat')

# Dictionary
ind_N=0
ind_r=1
ind_F=2
ind_mean_ttrans=3
ind_std_ttrans=4
ind_mean_tlastthread=5
ind_std_tlastthread=6
ind_mean_texit=7
ind_std_texit=8
ind_mean_nexit=9
ind_std_nexit=10
ind_mean_tfill=11
ind_std_tfill=12
ind_mean_tstuck=13
ind_std_tstuck=14
ind_mean_nstuck=15
ind_std_nstuck=16
ind_mean_ntail=17
ind_std_ntail=18
ind_failrate=19
ind_mean_tfail=20
ind_std_tfail=21









### Case Filters

Filter_RadOf4pt0_ForceOf0pt4 = np.where( (abs(A[:,ind_r]-4.0)<0.01) & (abs(A[:,ind_F]-0.4)<0.01) )
Filter_RadOf4pt0_ForceOf0pt5 = np.where( (abs(A[:,ind_r]-4.0)<0.01) & (abs(A[:,ind_F]-0.5)<0.01) )
Filter_RadOf4pt0_ForceOf0pt6 = np.where( (abs(A[:,ind_r]-4.0)<0.01) & (abs(A[:,ind_F]-0.6)<0.01) )
Filter_RadOf4pt0_ForceOf0pt75 = np.where( (abs(A[:,ind_r]-4.0)<0.01) & (abs(A[:,ind_F]-0.75)<0.01) )
Filter_RadOf4pt0_ForceOf1pt0 = np.where( (abs(A[:,ind_r]-4.0)<0.01) & (abs(A[:,ind_F]-1.0)<0.01) )
Filter_RadOf4pt0_ForceOf1pt25 = np.where( (abs(A[:,ind_r]-4.0)<0.01) & (abs(A[:,ind_F]-1.25)<0.01) )
Filter_RadOf4pt0_ForceOf1pt5 = np.where( (abs(A[:,ind_r]-4.0)<0.01) & (abs(A[:,ind_F]-1.5)<0.01) )
Filter_RadOf4pt0_ForceOf1pt6 = np.where( (abs(A[:,ind_r]-4.0)<0.01) & (abs(A[:,ind_F]-1.6)<0.01) )
Filter_RadOf4pt0_ForceOf1pt7 = np.where( (abs(A[:,ind_r]-4.0)<0.01) & (abs(A[:,ind_F]-1.7)<0.01) )
Filter_RadOf4pt0_ForceOf1pt8 = np.where( (abs(A[:,ind_r]-4.0)<0.01) & (abs(A[:,ind_F]-1.8)<0.01) )
Filter_RadOf4pt0_ForceOf1pt9 = np.where( (abs(A[:,ind_r]-4.0)<0.01) & (abs(A[:,ind_F]-1.9)<0.01) )
Filter_RadOf4pt0_ForceOf2pt0 = np.where( (abs(A[:,ind_r]-4.0)<0.01) & (abs(A[:,ind_F]-2.0)<0.01) )
Filter_RadOf4pt0_ForceOf3pt0 = np.where( (abs(A[:,ind_r]-4.0)<0.01) & (abs(A[:,ind_F]-3.0)<0.01) )
Filter_RadOf4pt0_ForceOf4pt0 = np.where( (abs(A[:,ind_r]-4.0)<0.01) & (abs(A[:,ind_F]-4.0)<0.01) )

Filter_RadOf3pt0_ForceOf0pt4 = np.where( (abs(A[:,ind_r]-3.0)<0.01) & (abs(A[:,ind_F]-0.4)<0.01) )
Filter_RadOf3pt0_ForceOf0pt5 = np.where( (abs(A[:,ind_r]-3.0)<0.01) & (abs(A[:,ind_F]-0.5)<0.01) )
Filter_RadOf3pt0_ForceOf0pt6 = np.where( (abs(A[:,ind_r]-3.0)<0.01) & (abs(A[:,ind_F]-0.6)<0.01) )
Filter_RadOf3pt0_ForceOf0pt75 = np.where( (abs(A[:,ind_r]-3.0)<0.01) & (abs(A[:,ind_F]-0.75)<0.01) )
Filter_RadOf3pt0_ForceOf1pt0 = np.where( (abs(A[:,ind_r]-3.0)<0.01) & (abs(A[:,ind_F]-1.0)<0.01) )
Filter_RadOf3pt0_ForceOf1pt25 = np.where( (abs(A[:,ind_r]-3.0)<0.01) & (abs(A[:,ind_F]-1.25)<0.01) )
Filter_RadOf3pt0_ForceOf1pt5 = np.where( (abs(A[:,ind_r]-3.0)<0.01) & (abs(A[:,ind_F]-1.5)<0.01) )

Filter_RadOf3pt5_ForceOf0pt4 = np.where( (abs(A[:,ind_r]-3.5)<0.01) & (abs(A[:,ind_F]-0.4)<0.01) )
Filter_RadOf3pt5_ForceOf0pt5 = np.where( (abs(A[:,ind_r]-3.5)<0.01) & (abs(A[:,ind_F]-0.5)<0.01) )
Filter_RadOf3pt5_ForceOf0pt6 = np.where( (abs(A[:,ind_r]-3.5)<0.01) & (abs(A[:,ind_F]-0.6)<0.01) )
Filter_RadOf3pt5_ForceOf0pt75 = np.where( (abs(A[:,ind_r]-3.5)<0.01) & (abs(A[:,ind_F]-0.75)<0.01) )
Filter_RadOf3pt5_ForceOf1pt0 = np.where( (abs(A[:,ind_r]-3.5)<0.01) & (abs(A[:,ind_F]-1.0)<0.01) )
Filter_RadOf3pt5_ForceOf1pt25 = np.where( (abs(A[:,ind_r]-3.5)<0.01) & (abs(A[:,ind_F]-1.25)<0.01) )
Filter_RadOf3pt5_ForceOf1pt5 = np.where( (abs(A[:,ind_r]-3.5)<0.01) & (abs(A[:,ind_F]-1.5)<0.01) )

Filter_RadOf4pt0_ForceOf0pt4 = np.where( (abs(A[:,ind_r]-4.0)<0.01) & (abs(A[:,ind_F]-0.4)<0.01) )
Filter_RadOf4pt0_ForceOf0pt5 = np.where( (abs(A[:,ind_r]-4.0)<0.01) & (abs(A[:,ind_F]-0.5)<0.01) )
Filter_RadOf4pt0_ForceOf0pt6 = np.where( (abs(A[:,ind_r]-4.0)<0.01) & (abs(A[:,ind_F]-0.6)<0.01) )
Filter_RadOf4pt0_ForceOf0pt75 = np.where( (abs(A[:,ind_r]-4.0)<0.01) & (abs(A[:,ind_F]-0.75)<0.01) )
Filter_RadOf4pt0_ForceOf1pt0 = np.where( (abs(A[:,ind_r]-4.0)<0.01) & (abs(A[:,ind_F]-1.0)<0.01) )
Filter_RadOf4pt0_ForceOf1pt25 = np.where( (abs(A[:,ind_r]-4.0)<0.01) & (abs(A[:,ind_F]-1.25)<0.01) )
Filter_RadOf4pt0_ForceOf1pt5 = np.where( (abs(A[:,ind_r]-4.0)<0.01) & (abs(A[:,ind_F]-1.5)<0.01) )
Filter_RadOf4pt0_ForceOf1pt6 = np.where( (abs(A[:,ind_r]-4.0)<0.01) & (abs(A[:,ind_F]-1.6)<0.01) )
Filter_RadOf4pt0_ForceOf1pt7 = np.where( (abs(A[:,ind_r]-4.0)<0.01) & (abs(A[:,ind_F]-1.7)<0.01) )
Filter_RadOf4pt0_ForceOf1pt8 = np.where( (abs(A[:,ind_r]-4.0)<0.01) & (abs(A[:,ind_F]-1.8)<0.01) )
Filter_RadOf4pt0_ForceOf1pt9 = np.where( (abs(A[:,ind_r]-4.0)<0.01) & (abs(A[:,ind_F]-1.9)<0.01) )
Filter_RadOf4pt0_ForceOf2pt0 = np.where( (abs(A[:,ind_r]-4.0)<0.01) & (abs(A[:,ind_F]-2.0)<0.01) )
Filter_RadOf4pt0_ForceOf3pt0 = np.where( (abs(A[:,ind_r]-4.0)<0.01) & (abs(A[:,ind_F]-3.0)<0.01) )
Filter_RadOf4pt0_ForceOf4pt0 = np.where( (abs(A[:,ind_r]-4.0)<0.01) & (abs(A[:,ind_F]-4.0)<0.01) )

FilterB_RadOf3pt0_ForceOf0pt4 = np.where( (abs(B[:,ind_r]-3.0)<0.01) & (abs(B[:,ind_F]-0.4)<0.01) )
FilterB_RadOf3pt0_ForceOf0pt5 = np.where( (abs(B[:,ind_r]-3.0)<0.01) & (abs(B[:,ind_F]-0.5)<0.01) )
FilterB_RadOf3pt0_ForceOf0pt6 = np.where( (abs(B[:,ind_r]-3.0)<0.01) & (abs(B[:,ind_F]-0.6)<0.01) )
FilterB_RadOf3pt0_ForceOf0pt75 = np.where( (abs(B[:,ind_r]-3.0)<0.01) & (abs(B[:,ind_F]-0.75)<0.01) )
FilterB_RadOf3pt0_ForceOf1pt0 = np.where( (abs(B[:,ind_r]-3.0)<0.01) & (abs(B[:,ind_F]-1.0)<0.01) )
FilterB_RadOf3pt0_ForceOf1pt25 = np.where( (abs(B[:,ind_r]-3.0)<0.01) & (abs(B[:,ind_F]-1.25)<0.01) )
FilterB_RadOf3pt0_ForceOf1pt5 = np.where( (abs(B[:,ind_r]-3.0)<0.01) & (abs(B[:,ind_F]-1.5)<0.01) )

FilterB_RadOf3pt5_ForceOf0pt4 = np.where( (abs(B[:,ind_r]-3.5)<0.01) & (abs(B[:,ind_F]-0.4)<0.01) )
FilterB_RadOf3pt5_ForceOf0pt5 = np.where( (abs(B[:,ind_r]-3.5)<0.01) & (abs(B[:,ind_F]-0.5)<0.01) )
FilterB_RadOf3pt5_ForceOf0pt6 = np.where( (abs(B[:,ind_r]-3.5)<0.01) & (abs(B[:,ind_F]-0.6)<0.01) )
FilterB_RadOf3pt5_ForceOf0pt75 = np.where( (abs(B[:,ind_r]-3.5)<0.01) & (abs(B[:,ind_F]-0.75)<0.01) )
FilterB_RadOf3pt5_ForceOf1pt0 = np.where( (abs(B[:,ind_r]-3.5)<0.01) & (abs(B[:,ind_F]-1.0)<0.01) )
FilterB_RadOf3pt5_ForceOf1pt25 = np.where( (abs(B[:,ind_r]-3.5)<0.01) & (abs(B[:,ind_F]-1.25)<0.01) )
FilterB_RadOf3pt5_ForceOf1pt5 = np.where( (abs(B[:,ind_r]-3.5)<0.01) & (abs(B[:,ind_F]-1.5)<0.01) )

FilterB_RadOf4pt0_ForceOf0pt4 = np.where( (abs(B[:,ind_r]-4.0)<0.01) & (abs(B[:,ind_F]-0.4)<0.01) )
FilterB_RadOf4pt0_ForceOf0pt5 = np.where( (abs(B[:,ind_r]-4.0)<0.01) & (abs(B[:,ind_F]-0.5)<0.01) )
FilterB_RadOf4pt0_ForceOf0pt6 = np.where( (abs(B[:,ind_r]-4.0)<0.01) & (abs(B[:,ind_F]-0.6)<0.01) )
FilterB_RadOf4pt0_ForceOf0pt75 = np.where( (abs(B[:,ind_r]-4.0)<0.01) & (abs(B[:,ind_F]-0.75)<0.01) )
FilterB_RadOf4pt0_ForceOf1pt0 = np.where( (abs(B[:,ind_r]-4.0)<0.01) & (abs(B[:,ind_F]-1.0)<0.01) )
FilterB_RadOf4pt0_ForceOf1pt25 = np.where( (abs(B[:,ind_r]-4.0)<0.01) & (abs(B[:,ind_F]-1.25)<0.01) )
FilterB_RadOf4pt0_ForceOf1pt5 = np.where( (abs(B[:,ind_r]-4.0)<0.01) & (abs(B[:,ind_F]-1.5)<0.01) )
FilterB_RadOf4pt0_ForceOf1pt6 = np.where( (abs(B[:,ind_r]-4.0)<0.01) & (abs(B[:,ind_F]-1.6)<0.01) )
FilterB_RadOf4pt0_ForceOf1pt7 = np.where( (abs(B[:,ind_r]-4.0)<0.01) & (abs(B[:,ind_F]-1.7)<0.01) )
FilterB_RadOf4pt0_ForceOf1pt8 = np.where( (abs(B[:,ind_r]-4.0)<0.01) & (abs(B[:,ind_F]-1.8)<0.01) )
FilterB_RadOf4pt0_ForceOf1pt9 = np.where( (abs(B[:,ind_r]-4.0)<0.01) & (abs(B[:,ind_F]-1.9)<0.01) )
FilterB_RadOf4pt0_ForceOf2pt0 = np.where( (abs(B[:,ind_r]-4.0)<0.01) & (abs(B[:,ind_F]-2.0)<0.01) )
FilterB_RadOf4pt0_ForceOf3pt0 = np.where( (abs(B[:,ind_r]-4.0)<0.01) & (abs(B[:,ind_F]-3.0)<0.01) )
FilterB_RadOf4pt0_ForceOf4pt0 = np.where( (abs(B[:,ind_r]-4.0)<0.01) & (abs(B[:,ind_F]-4.0)<0.01) )


### Function Definitions

## 
def PlotMany_ttrans(A,Filters):
        # # Label positions (rnom13)
        # LabelPosN = np.array([15.,11.,11.,11.,11.,11.])
        # LabelPosy = np.array([1.9e5,1.05e5,5.5e4,2.7e4,7.e3,2.5e3])
        # LabelAngles = np.array([-11,-10,-10,-8,3,5])
        # Label positions (rnom15)
        LabelPosN = np.array([15.,11.,11.,11.,11.])
        LabelPosy = np.array([2.2e4,1.6e4,7.2e3,3.7e3,1.7e3])
        LabelAngles = np.array([-12,-11,-5,0,5])
	# Iterate over the filters
        for j in range(len(Filters)):
            Filter = Filters[j]
            B = A[Filter]
            last_plot = plt.loglog(B[:,ind_N],B[:,ind_mean_ttrans],'-*',lw=3)
            # Labels
            Labely = LabelPosy[j]
            LabelN = LabelPosN[j]
#            plt.text(LabelN,Labely,"F = %.2f"%B[0,ind_F],color=last_plot[0].get_color(),fontsize=fontsize_innerlabels,rotation=LabelAngles[j])
        # Arrows indicating force trends
        opt = dict(color='k',width=0.5)
#        plt.annotate('Increasing F',xytext=(40,2.e4),xy=(300,2.5e3),fontsize=fontsize_innerlabels,arrowprops=opt)
	# Set titles
        plt.xlabel('$N$',fontsize=fontsize_axislabels)
        plt.ylabel('$t_{\mathrm{trans}}$',fontsize=fontsize_axislabels)
	# Set limits
        plt.xlim([10,400])
        #plt.ylim([2e3,3e4])
        plt.ylim([2.e3,3e5])
        # Extra ticks
        extra_ticks = np.concatenate([np.arange(20,100,20),np.arange(100,400+1,100)],axis=1)
        #plt.gca().get_xaxis().set_ticks(extra_ticks)
        #plt.gca().get_xaxis().set_ticklabels(extra_ticks)
        matplotlib.rcParams['xtick.labelsize'] = fontsize_ticks
        matplotlib.rcParams['ytick.labelsize'] = fontsize_ticks
	# Display plot
	#plt.show()
# End of function #

## 
def PlotMany_tstuck(A,Filters):
	# Iterate over the filters
        for Filter in Filters:
		B = A[Filter]
                last_plot = plt.loglog(B[:,ind_N],B[:,ind_mean_tstuck],'-*',lw=3)
        # Arrows indicating force trends
        opt = dict(color='k',width=0.5)
        #plt.annotate('Increasing F',xytext=(35,1.7e5),xy=(300,2e3),fontsize=fontsize_innerlabels,arrowprops=opt)
        #plt.annotate('Increasing F',xytext=(35,1.7e4),xy=(170,1e3),fontsize=fontsize_innerlabels,arrowprops=opt)
	# Set titles
        plt.xlabel('$N$',fontsize=fontsize_axislabels)
        plt.ylabel('$t_{\mathrm{stuck}}$',fontsize=fontsize_axislabels)
	# Set limits
        plt.xlim([10,400])
        plt.ylim([1e3,3e5])
        # Extra ticks
        matplotlib.rcParams['xtick.labelsize'] = fontsize_ticks
        matplotlib.rcParams['ytick.labelsize'] = fontsize_ticks
	# Display plot
	#plt.show()
# End of function #

## 
def PlotMany_timedivvy(A,Filters):
	# Iterate over the filters
        for Filter in Filters:
		B = A[Filter]
                last_plot = plt.loglog(B[:,ind_N],B[:,ind_mean_ttrans],'-*',lw=3)
                plt.loglog(B[:,ind_N],B[:,ind_mean_tstuck],'-*',lw=3)
                plt.loglog(B[:,ind_N],B[:,ind_mean_tstuck]+B[:,ind_mean_texit],'-*',lw=3)
        # Arrows indicating force trends
        opt = dict(color='k',width=0.5)
        #plt.annotate('Increasing F',xytext=(35,1.7e5),xy=(300,2e3),fontsize=fontsize_innerlabels,arrowprops=opt)
        #plt.annotate('Increasing F',xytext=(35,1.7e4),xy=(170,1e3),fontsize=fontsize_innerlabels,arrowprops=opt)
	# Set titles
        plt.xlabel('$N$',fontsize=fontsize_axislabels)
        plt.ylabel('$t_{\mathrm{stuck}}$',fontsize=fontsize_axislabels)
	# Set limits
        plt.xlim([10,400])
        plt.ylim([1e3,3e5])
        # Extra ticks
        matplotlib.rcParams['xtick.labelsize'] = fontsize_ticks
        matplotlib.rcParams['ytick.labelsize'] = fontsize_ticks
	# Display plot
	#plt.show()
# End of function #

## 
def PlotMany_tstuckVnstuck(A,Filters):
	# Iterate over the filters
        for Filter in Filters:
		B = A[Filter]
                last_plot = plt.loglog(B[:,ind_mean_nstuck],B[:,ind_mean_tstuck],'-*',lw=3)
        # Arrows indicating force trends
        opt = dict(color='k',width=0.5)
        plt.annotate('Increasing F',xytext=(35,1.7e5),xy=(300,2e3),fontsize=fontsize_innerlabels,arrowprops=opt)
        #plt.annotate('Increasing F',xytext=(35,1.7e4),xy=(170,1e3),fontsize=fontsize_innerlabels,arrowprops=opt)
	# Set titles
        plt.xlabel('$N$',fontsize=fontsize_axislabels)
        plt.ylabel('$t_{\mathrm{stuck}}$',fontsize=fontsize_axislabels)
	# Set limits
        plt.xlim([10,400])
        plt.ylim([1e3,3e5])
        # Extra ticks
        matplotlib.rcParams['xtick.labelsize'] = fontsize_ticks
        matplotlib.rcParams['ytick.labelsize'] = fontsize_ticks
	# Display plot
	#plt.show()
# End of function #

## 
def PlotMany_texit(A,Filters):
    # Iterate over the filters
        for Filter in Filters:
		B = A[Filter]
                last_plot = plt.loglog(B[:,ind_N],B[:,ind_mean_texit],'-*',lw=3)
        # Arrows indicating force trends
        opt = dict(color='k',width=0.5)
        #plt.annotate('Increasing F',xytext=(12,2e3),xy=(250,40),fontsize=fontsize_innerlabels,arrowprops=opt)
	# Set titles
        plt.xlabel('$N$',fontsize=fontsize_axislabels)
        plt.ylabel('$t_{\mathrm{exit}}$',fontsize=fontsize_axislabels)
	# Set limits
        plt.xlim([10,400])
        plt.ylim([2e1,2e4])
	# Scaling Law
	Ns = np.arange(10,400,1)
        SNprefac=3.71
        SNalpha=1.47
        #plt.loglog(Ns,1.5*(5./3.)*(Ns)**(SNalpha),'k--')
        plt.loglog(Ns,SNprefac*(Ns)**(SNalpha),'k--')
        #SNprefac=2.57
        #SNalpha=1.52
        #plt.loglog(Ns,(5./3.)*(Ns)**(SNalpha),'k--')
        #plt.text(50.,8.e3,r'$\tau \,=\, %.2f\, N^{\,%.2f}$'%(SNprefac,SNalpha),color='k',fontsize=fontsize_innerlabels)
        #plt.text(20.,2.e3,'Standard Nanopore Scaling',color='k',fontsize=fontsize_innerlabels)
        plt.annotate('Standard Nanopore Scaling',xytext=(12.,3.e3),xy=(60,1.2e3),fontsize=fontsize_innerlabels,arrowprops=opt)
        # Extra ticks
        extra_ticks = np.concatenate([np.arange(20,100,20),np.arange(100,400+1,100)],axis=1)
        #plt.gca().get_xaxis().set_ticks(extra_ticks)
        #plt.gca().get_xaxis().set_ticklabels(extra_ticks)
        matplotlib.rcParams['xtick.labelsize'] = fontsize_ticks
        matplotlib.rcParams['ytick.labelsize'] = fontsize_ticks
	# Display plot
	#plt.show()
# End of function #

## 
def PlotMany_failrate(A,Filters):
    # Iterate over the filters
        for Filter in Filters:
		B = A[Filter]
                last_plot = plt.semilogx(B[:,ind_N],100*B[:,ind_failrate],'-*',lw=3)
        # Arrows indicating force trends
        opt = dict(color='k',width=0.5)
        plt.annotate('Increasing F',xytext=(12,85),xy=(70,0.5),fontsize=fontsize_innerlabels,arrowprops=opt)
	# Set titles
        plt.xlabel('$N$',fontsize=fontsize_axislabels)
        plt.ylabel('$\mathrm{Failrate\,[\%]}$',fontsize=fontsize_axislabels)
	# Set limits
        plt.xlim([10,400])
        plt.ylim([0,100])
        # Extra ticks
        extra_ticks = np.concatenate([np.arange(20,100,20),np.arange(100,400+1,100)],axis=1)
        #plt.gca().get_xaxis().set_ticks(extra_ticks)
        #plt.gca().get_xaxis().set_ticklabels(extra_ticks)
        matplotlib.rcParams['xtick.labelsize'] = fontsize_ticks
        matplotlib.rcParams['ytick.labelsize'] = fontsize_ticks
	# Display plot
	#plt.show()
# End of function #

## 
def PlotMany_nstuck(A,Filters):
    # Iterate over the filters
        for Filter in Filters:
		B = A[Filter]
                last_plot = plt.semilogx(B[:,ind_N],B[:,ind_mean_nstuck],'-*',lw=3)
                Ncrit = B[np.argmax(B[:,ind_mean_nstuck]),ind_N]
                Ncrit_alt = B[np.argmin(B[:,ind_mean_ttrans]),ind_N]
                print "%.1f %.1f %d %d" % (B[0,ind_r],B[0,ind_F],Ncrit,Ncrit_alt)
        # Arrows indicating force trends
        opt = dict(color='k',width=0.5)
        #plt.annotate('Increasing F',xytext=(35,20),xy=(350,155),fontsize=fontsize_innerlabels,arrowprops=opt)
        #plt.annotate('Increasing F',xytext=(35,20),xy=(350,105),fontsize=fontsize_innerlabels,arrowprops=opt)
	# Set titles
        plt.xlabel('$N$',fontsize=fontsize_axislabels)
        plt.ylabel('$N_{\mathrm{stuck}}$',fontsize=fontsize_axislabels)
	# Set limits
        plt.xlim([10,400])
        plt.ylim([10,160])
        # Scaling Law
	Ns = np.arange(10,400,1)
        plt.semilogx(Ns,Ns,'k-')
#        plt.text(25.,55.,"$N_{\mathrm{stuck}} = N$",
#                        color='k',fontsize=fontsize_innerlabels)
        plt.annotate("$N_{\mathrm{stuck}} = N$",xytext=(25,55),xy=(68,68),fontsize=fontsize_innerlabels,arrowprops=opt)
        # Extra ticks
        # extra_yticks = np.array([10,40,70,100,130,160])
        # plt.gca().get_yaxis().set_ticks(extra_yticks)
        # plt.gca().get_yaxis().set_ticklabels(extra_yticks)
        # matplotlib.rcParams['xtick.labelsize'] = fontsize_ticks
        # matplotlib.rcParams['ytick.labelsize'] = fontsize_ticks
	# Display plot
	#plt.show()
# End of function #

## 
def PlotMany_nexit(A,Filters):
    # Iterate over the filters
        for Filter in Filters:
		B = A[Filter]
                last_plot = plt.semilogx(B[:,ind_N],B[:,ind_mean_nexit],'-*',lw=3)
        # Arrows indicating force trends
        opt = dict(color='k',width=0.5)
        #plt.annotate('Increasing F',xytext=(35,10),xy=(300,70),fontsize=fontsize_innerlabels,arrowprops=opt)
        plt.annotate('Increasing F',xytext=(45,12),xy=(300,55),fontsize=fontsize_innerlabels,arrowprops=opt)
	# Set titles
        plt.xlabel('$N$',fontsize=fontsize_axislabels)
        plt.ylabel('$N_{\mathrm{exit}}$',fontsize=fontsize_axislabels)
	# Set limits
        plt.xlim([10,400])
        plt.ylim([5,75])
        # Scaling Law
	Ns = np.linspace(10,200,1000)
        plt.semilogx(Ns,Ns/2,'k--')
        #plt.text(20.,40.,"$N_{\mathrm{exit}} = N/2$",
        #                color='k',fontsize=fontsize_innerlabels)
        #plt.annotate("$N_{\mathrm{exit}} = N/2$",xytext=(40,50),xy=(120,60),fontsize=fontsize_innerlabels,arrowprops=opt)
        plt.annotate("$N_{\mathrm{exit}} = N/2$",xytext=(30,40),xy=(100,50),fontsize=fontsize_innerlabels,arrowprops=opt)
        # Extra ticks
        extra_yticks = np.array([10,25,40,55,70])
        plt.gca().get_yaxis().set_ticks(extra_yticks)
        plt.gca().get_yaxis().set_ticklabels(extra_yticks)
        matplotlib.rcParams['xtick.labelsize'] = fontsize_ticks
        matplotlib.rcParams['ytick.labelsize'] = fontsize_ticks
	# Display plot
	#plt.show()
# End of function #

## 
def PlotMany_ntail(A,Filters):
    # Iterate over the filters
        for Filter in Filters:
		B = A[Filter]
                last_plot = plt.semilogx(B[:,ind_N],B[:,ind_mean_ntail],'-*',lw=3)
        # Arrows indicating force trends
        opt = dict(color='k',width=0.5)
        #plt.annotate('Increasing F',xytext=(50,100),xy=(275,50),fontsize=fontsize_innerlabels,arrowprops=opt)
	# Set titles
        plt.xlabel('$N$',fontsize=fontsize_axislabels)
        plt.ylabel('$N_{\mathrm{tail}}$',fontsize=fontsize_axislabels)
	# Set limits
        #plt.xlim([10,400])
        #plt.ylim([0,400])
        # # Scaling Law
	# Ns = np.arange(10,400,1)
        # plt.semilogx(Ns,Ns/2,'k--')
        # plt.text(35.,35.,"$N_{\mathrm{exit}} = N/2$",
        #                 color='k',fontsize=fontsize_innerlabels)
#        plt.annotate("$N_{\mathrm{stuck}} = N$",xytext=(25,55),xy=(68,68),fontsize=fontsize_innerlabels,arrowprops=opt)
        # Extra ticks
        extra_ticks = np.concatenate([np.arange(20,100,20),np.arange(100,400+1,100)],axis=1)
        #plt.gca().get_xaxis().set_ticks(extra_ticks)
        #plt.gca().get_xaxis().set_ticklabels(extra_ticks)
        matplotlib.rcParams['xtick.labelsize'] = fontsize_ticks
        matplotlib.rcParams['ytick.labelsize'] = fontsize_ticks
	# Display plot
	#plt.show()
# End of function #

## 
def PlotMany_NcritComp(A,Filters):
    # Iterate over the filters
        color_by_radius = ['r','g','b']
        marker_by_radius = ['*','^','o']
        for Filter in Filters:
		B = A[Filter]
                Ncrit_Nstuck = B[np.argmax(B[:,ind_mean_nstuck]),ind_N]
                Ncrit_Ttrans = B[np.argmin(B[:,ind_mean_ttrans]),ind_N]
                if (B[1,ind_F]==0.4):
                        plt.plot(Ncrit_Nstuck,Ncrit_Ttrans,'*',markersize=15,
                                 marker=marker_by_radius[int(round(B[0,ind_r]/0.5))-6],
                                 markerfacecolor='none',
                                 markeredgecolor=color_by_radius[int(round(B[0,ind_r]/0.5))-6],
                                 markeredgewidth=3,
                                 label='$r_{\mathrm{eff}}$ = %.1f'%B[1,ind_r])
                else:
                        plt.plot(Ncrit_Nstuck,Ncrit_Ttrans,'*',markersize=15,
                                 marker=marker_by_radius[int(round(B[0,ind_r]/0.5))-6],
                                 markerfacecolor='none',
                                 markeredgecolor=color_by_radius[int(round(B[0,ind_r]/0.5))-6],
                                 markeredgewidth=3)
        # Arrows indicating force trends
        opt = dict(color='k',width=0.5)
        plt.annotate('Increasing F',xytext=(20,10),xy=(120,80),fontsize=fontsize_innerlabels,arrowprops=opt)
	# Set titles
        #plt.xlabel('$N_{\mathrm{meas}}^*$',fontsize=fontsize_axislabels)
        #plt.ylabel('$N_{\mathrm{true}}^*$',fontsize=fontsize_axislabels)
        plt.xlabel('$N$ at $\max(N_{\mathrm{stuck}})$',fontsize=fontsize_axislabels)
        plt.ylabel('$N$ at $\min(t_{\mathrm{trans}})$',fontsize=fontsize_axislabels)
	# Set limits
        plt.xlim([0,150])
        plt.ylim([0,150])
        # Scaling Law
	Ns = np.arange(0,200,1)
        plt.plot(Ns,Ns,'k--',lw=3)
	# Display plot
        plt.legend(numpoints=1,loc='upper left')
#	plt.show()
# End of function #

##
def PlotSixPanel(A,Filters):
    f, axarr = plt.subplots(3,2)
    plt.sca(axarr[0,0])
    PlotMany_ttrans(A,Filters)
    plt.text(250,1.5e5,'(a)',fontsize=fontsize_innerlabels)
    plt.xlabel('')
    plt.gca().get_xaxis().set_ticklabels([])
    plt.sca(axarr[1,0])
    PlotMany_tstuck(A,Filters)
    plt.text(250,1.5e5,'(b)',fontsize=fontsize_innerlabels)
    plt.xlabel('')
    plt.gca().get_xaxis().set_ticklabels([])
    plt.sca(axarr[2,0])
    PlotMany_texit(A,Filters)
    plt.text(12,8e3,'(c)',fontsize=fontsize_innerlabels)
    plt.xlabel('')
    plt.text(50,6.5,'$N$',fontsize=fontsize_axislabels)

    plt.sca(axarr[0,1])
    PlotMany_nstuck(A,Filters)
    plt.text(12,142,'(d)',fontsize=fontsize_innerlabels)
    plt.xlabel('')
    plt.gca().get_xaxis().set_ticklabels([])
    plt.sca(axarr[1,1])
    PlotMany_nexit(A,Filters)
    plt.text(12,66,'(e)',fontsize=fontsize_innerlabels)
    plt.xlabel('')
    plt.text(50,-3,'$N$',fontsize=fontsize_axislabels)
    plt.sca(axarr[2,1])
    plt.axis('off')

    plt.subplots_adjust(left=0.10, bottom=0.07, right=0.98, top=0.98,
                        wspace=0.25, hspace=0.08)
    plt.show()
# End of function #

matplotlib.rcParams['xtick.labelsize'] = fontsize_ticks
matplotlib.rcParams['ytick.labelsize'] = fontsize_ticks

# plt.figure(figsize=(10,6))
# PlotMany_ttrans(A,[Filter_RadOf4pt0_ForceOf0pt4])
# plt.xlabel('')
# plt.text(46,3.8e4,'$N$',fontsize=fontsize_axislabels)
# plt.ylabel('')
# plt.text(7,1.2e5,'$t_{\mathrm{trans}}$',fontsize=fontsize_axislabels,rotation=90)
# plt.xlim([10,200])
# extra_ticks = np.concatenate([np.arange(20,100,20),np.arange(100,200+1,100)],axis=1)
# plt.gca().get_xaxis().set_ticks(extra_ticks)
# plt.gca().get_xaxis().set_ticklabels(extra_ticks)
# plt.ylim([4.5e4,2.4e5])
# plt.gca().get_yaxis().set_ticks([5e4,6e4,7e4,8e4,9e4,1e5,2e5])
# plt.gca().get_yaxis().set_ticklabels([r'$5\times 10^4$',r'$6\times 10^4$',r'$7\times 10^4$',
#                                       r'$8\times 10^4$',r'$9\times 10^4$'
#                                       ,r'$10^5$',r'$2\times 10^5$'])
# plt.subplots_adjust(left=0.12, bottom=0.10, right=0.95, top=0.98,
#                     wspace=0.20, hspace=0.20)
# plt.show()


# plt.figure(figsize=(10,6))
# PlotMany_ttrans(A,[Filter_RadOf4pt0_ForceOf0pt4,
# Filter_RadOf3pt0_ForceOf0pt4,
# ])
# plt.xlabel('')
# plt.text(46,8e3,'$N$',fontsize=fontsize_axislabels)
# plt.ylabel('')
# plt.text(8,5e4,'$t_{\mathrm{trans}}$',fontsize=fontsize_axislabels,rotation=90)
# plt.xlim([10,200])
# extra_ticks = np.concatenate([np.arange(20,100,20),np.arange(100,200+1,100)],axis=1)
# plt.gca().get_xaxis().set_ticks(extra_ticks)
# plt.gca().get_xaxis().set_ticklabels(extra_ticks)
# plt.ylim([1e4,2.4e5])
# plt.text(30,1.5e5,'$r_{\mathrm{eff,cavi}} = 4.0\sigma$',fontsize=fontsize_innerlabels,color='b')
# plt.text(13,3e4,'$r_{\mathrm{eff,cavi}} = 3.0\sigma$',fontsize=fontsize_innerlabels,color='g')
# plt.subplots_adjust(left=0.12, bottom=0.10, right=0.95, top=0.98,
#                     wspace=0.20, hspace=0.20)
# plt.show()


# plt.figure(figsize=(10,6))
# PlotMany_ttrans(A,[Filter_RadOf4pt0_ForceOf3pt0])
# plt.xlabel('')
# plt.text(46,1.3e3,'$N$',fontsize=fontsize_axislabels)
# plt.ylabel('')
# plt.text(7,3e3,'$t_{\mathrm{trans}}$',fontsize=fontsize_axislabels,rotation=90)
# plt.xlim([10,400])
# extra_ticks = np.concatenate([np.arange(20,100,20),np.arange(100,400+1,100)],axis=1)
# plt.gca().get_xaxis().set_ticks(extra_ticks)
# plt.gca().get_xaxis().set_ticklabels(extra_ticks)
# plt.ylim([1.5e3,9.5e3])
# plt.gca().get_yaxis().set_ticks([2e3,4e3,6e3,8e3])
# plt.gca().get_yaxis().set_ticklabels([r'$2\times 10^3$',r'$4\times 10^3$',r'$6\times 10^3$',
#                                       r'$8\times 10^3$'])
# plt.subplots_adjust(left=0.12, bottom=0.10, right=0.95, top=0.98,
#                     wspace=0.20, hspace=0.20)
# plt.show()


# plt.figure(figsize=(10,6))
# PlotMany_ttrans(A,[Filter_RadOf4pt0_ForceOf0pt4,
# Filter_RadOf4pt0_ForceOf0pt5,
# Filter_RadOf4pt0_ForceOf0pt75,
# Filter_RadOf4pt0_ForceOf1pt0,
# Filter_RadOf4pt0_ForceOf2pt0,
# Filter_RadOf4pt0_ForceOf3pt0
# ])
# plt.xlabel('')
# plt.text(46,1.2e3,'$N$',fontsize=fontsize_axislabels)
# plt.ylabel('')
# plt.text(7,1e4,'$t_{\mathrm{trans}}$',fontsize=fontsize_axislabels,rotation=90)
# plt.xlim([10,400])
# extra_ticks = np.concatenate([np.arange(20,100,20),np.arange(100,400+1,100)],axis=1)
# plt.gca().get_xaxis().set_ticks(extra_ticks)
# plt.gca().get_xaxis().set_ticklabels(extra_ticks)
# plt.ylim([1.8e3,2.8e5])
# plt.gca().get_yaxis().set_ticks([2e3,4e3,1e4,2e4,4e4,1e5,2e5])
# plt.gca().get_yaxis().set_ticklabels([r'$2\times 10^3$',r'$4\times 10^3$',r'$10^4$',
#                                       r'$2\times 10^4$',r'$4\times 10^4$'
#                                       ,r'$10^5$',r'$2\times 10^5$'])
# plt.subplots_adjust(left=0.10, bottom=0.10, right=0.95, top=0.98,
#                     wspace=0.20, hspace=0.20)
# plt.show()


# plt.figure(figsize=(10,6))
# PlotMany_tstuck(A,[Filter_RadOf4pt0_ForceOf0pt4])
# plt.xlabel('')
# plt.text(46,3.8e4,'$N$',fontsize=fontsize_axislabels)
# plt.ylabel('')
# plt.text(7,1.2e5,'$t_{\mathrm{stuck}}$',fontsize=fontsize_axislabels,rotation=90)
# plt.xlim([10,200])
# extra_ticks = np.concatenate([np.arange(20,100,20),np.arange(100,200+1,100)],axis=1)
# plt.gca().get_xaxis().set_ticks(extra_ticks)
# plt.gca().get_xaxis().set_ticklabels(extra_ticks)
# plt.ylim([4.5e4,2.4e5])
# plt.gca().get_yaxis().set_ticks([5e4,6e4,7e4,8e4,9e4,1e5,2e5])
# plt.gca().get_yaxis().set_ticklabels([r'$5\times 10^4$',r'$6\times 10^4$',r'$7\times 10^4$',
#                                       r'$8\times 10^4$',r'$9\times 10^4$'
#                                       ,r'$10^5$',r'$2\times 10^5$'])
# plt.subplots_adjust(left=0.12, bottom=0.10, right=0.95, top=0.98,
#                     wspace=0.20, hspace=0.20)
# plt.show()


# plt.figure(figsize=(10,6))
# PlotMany_tstuck(A,[Filter_RadOf4pt0_ForceOf0pt4,
# Filter_RadOf4pt0_ForceOf0pt5,
# Filter_RadOf4pt0_ForceOf0pt75,
# Filter_RadOf4pt0_ForceOf1pt0,
# Filter_RadOf4pt0_ForceOf2pt0,
# Filter_RadOf4pt0_ForceOf3pt0
# ])
# plt.xlabel('')
# plt.text(46,9.5e2,'$N$',fontsize=fontsize_axislabels)
# plt.ylabel('')
# plt.text(7,1e4,'$t_{\mathrm{stuck}}$',fontsize=fontsize_axislabels,rotation=90)
# plt.xlim([10,400])
# extra_ticks = np.concatenate([np.arange(20,100,20),np.arange(100,400+1,100)],axis=1)
# plt.gca().get_xaxis().set_ticks(extra_ticks)
# plt.gca().get_xaxis().set_ticklabels(extra_ticks)
# plt.ylim([1.5e3,2.8e5])
# plt.gca().get_yaxis().set_ticks([2e3,4e3,1e4,2e4,4e4,1e5,2e5])
# plt.gca().get_yaxis().set_ticklabels([r'$2\times 10^3$',r'$4\times 10^3$',r'$10^4$',
#                                       r'$2\times 10^4$',r'$4\times 10^4$'
#                                       ,r'$10^5$',r'$2\times 10^5$'])
# plt.subplots_adjust(left=0.10, bottom=0.10, right=0.95, top=0.98,
#                     wspace=0.20, hspace=0.20)
# plt.show()


# plt.figure(figsize=(10,6))
# PlotMany_tstuck(A,[Filter_RadOf4pt0_ForceOf2pt0,
# Filter_RadOf4pt0_ForceOf3pt0,
# Filter_RadOf4pt0_ForceOf4pt0])
# plt.xlabel('')
# plt.text(46,9.5e2,'$N$',fontsize=fontsize_axislabels)
# plt.ylabel('')
# plt.text(7,1e4,'$t_{\mathrm{stuck}}$',fontsize=fontsize_axislabels,rotation=90)
# plt.xlim([10,400])
# extra_ticks = np.concatenate([np.arange(20,100,20),np.arange(100,400+1,100)],axis=1)
# plt.gca().get_xaxis().set_ticks(extra_ticks)
# plt.gca().get_xaxis().set_ticklabels(extra_ticks)
# plt.ylim([1.5e3,2.8e5])
# plt.gca().get_yaxis().set_ticks([2e3,4e3,1e4,2e4,4e4,1e5,2e5])
# plt.gca().get_yaxis().set_ticklabels([r'$2\times 10^3$',r'$4\times 10^3$',r'$10^4$',
#                                       r'$2\times 10^4$',r'$4\times 10^4$'
#                                       ,r'$10^5$',r'$2\times 10^5$'])
# plt.subplots_adjust(left=0.10, bottom=0.10, right=0.95, top=0.98,
#                     wspace=0.20, hspace=0.20)
# plt.show()


# plt.figure(figsize=(10,6))
# PlotMany_tstuckVnstuck(A,[Filter_RadOf4pt0_ForceOf0pt4,
# Filter_RadOf4pt0_ForceOf0pt5,
# Filter_RadOf4pt0_ForceOf0pt75,
# Filter_RadOf4pt0_ForceOf1pt0,
# Filter_RadOf4pt0_ForceOf2pt0,
# Filter_RadOf4pt0_ForceOf3pt0
# ])
# plt.xlabel('')
# plt.text(46,9.5e2,'$N_{\mathrm{stuck}}$',fontsize=fontsize_axislabels)
# plt.ylabel('')
# plt.text(7,1e4,'$t_{\mathrm{stuck}}$',fontsize=fontsize_axislabels,rotation=90)
# plt.xlim([10,100])
# extra_ticks = np.concatenate([np.arange(20,100,20),np.arange(100,200+1,100)],axis=1)
# plt.gca().get_xaxis().set_ticks(extra_ticks)
# plt.gca().get_xaxis().set_ticklabels(extra_ticks)
# plt.ylim([1.5e3,2.8e5])
# plt.gca().get_yaxis().set_ticks([2e3,4e3,1e4,2e4,4e4,1e5,2e5])
# plt.gca().get_yaxis().set_ticklabels([r'$2\times 10^3$',r'$4\times 10^3$',r'$10^4$',
#                                       r'$2\times 10^4$',r'$4\times 10^4$'
#                                       ,r'$10^5$',r'$2\times 10^5$'])
# plt.subplots_adjust(left=0.12, bottom=0.12, right=0.95, top=0.98,
#                     wspace=0.20, hspace=0.20)
# plt.show()


# plt.figure(figsize=(10,6))
# PlotMany_tstuckVnstuck(A,[Filter_RadOf4pt0_ForceOf0pt4,
# Filter_RadOf4pt0_ForceOf0pt5,
# Filter_RadOf4pt0_ForceOf0pt75,
# ])
# plt.xlabel('')
# plt.text(25,8.e3,'$N_{\mathrm{stuck}}$',fontsize=fontsize_axislabels)
# plt.ylabel('')
# plt.text(7.5,1e5,'$t_{\mathrm{stuck}}$',fontsize=fontsize_axislabels,rotation=90)
# plt.xlim([10,100])
# extra_ticks = np.concatenate([np.arange(20,100,20),np.arange(100,100+1,100)],axis=1)
# plt.gca().get_xaxis().set_ticks(extra_ticks)
# plt.gca().get_xaxis().set_ticklabels(extra_ticks)
# plt.ylim([1.e4,2.8e5])
# plt.gca().get_yaxis().set_ticks([1e4,2e4,4e4,1e5,2e5])
# plt.gca().get_yaxis().set_ticklabels([r'$10^4$',
#                                       r'$2\times 10^4$',r'$4\times 10^4$'
#                                       ,r'$10^5$',r'$2\times 10^5$'])
# plt.subplots_adjust(left=0.12, bottom=0.10, right=0.95, top=0.98,
#                     wspace=0.20, hspace=0.20)
# plt.show()


# plt.figure(figsize=(10,6))
# PlotMany_tstuckVnstuck(A,[Filter_RadOf4pt0_ForceOf0pt4])
# plt.loglog(47,1.25e5,'o',markersize=50,markeredgewidth=3,markeredgecolor='k',markerfacecolor='none') # Limit point
# plt.xlabel('')
# plt.text(25,4.e4,'$N_{\mathrm{stuck}}$',fontsize=fontsize_axislabels)
# plt.ylabel('')
# plt.text(7.5,1.3e5,'$t_{\mathrm{stuck}}$',fontsize=fontsize_axislabels,rotation=90)
# plt.xlim([10,90])
# extra_ticks = np.concatenate([np.arange(20,100,20),np.arange(100,80+1,100)],axis=1)
# plt.gca().get_xaxis().set_ticks(extra_ticks)
# plt.gca().get_xaxis().set_ticklabels(extra_ticks)
# plt.ylim([4.5e4,2.4e5])
# plt.gca().get_yaxis().set_ticks([5e4,6e4,7e4,8e4,9e4,1e5,2e5])
# plt.gca().get_yaxis().set_ticklabels([r'$5\times 10^4$',r'$6\times 10^4$',r'$7\times 10^4$',
#                                       r'$8\times 10^4$',r'$9\times 10^4$'
#                                       ,r'$10^5$',r'$2\times 10^5$'])
# opt = dict(color='k',width=0.5)
# plt.annotate("Increasing $N$", xytext=(11,1.3e5), xy=(55,5.1e4), fontsize=fontsize_innerlabels,arrowprops=opt)
# plt.annotate("$N = 70$", xytext=(70,6.e4), xy=(67,5.1e4), fontsize=fontsize_innerlabels,arrowprops=opt)
# plt.annotate("$N = 80$", xytext=(62,7e4), xy=(58,6.2e4), fontsize=fontsize_innerlabels,arrowprops=opt)
# plt.annotate("$N = 85$", xytext=(62,9e4), xy=(52,9e4), fontsize=fontsize_innerlabels,arrowprops=opt)
# plt.annotate("$N \in [90,200]$", xytext=(62,1.5e5), xy=(52,1.3e5), fontsize=fontsize_innerlabels,arrowprops=opt)
# plt.subplots_adjust(left=0.12, bottom=0.10, right=0.95, top=0.98,
#                     wspace=0.20, hspace=0.20)
# plt.show()


# plt.figure(figsize=(10,6))
# PlotMany_nstuck(A,[Filter_RadOf4pt0_ForceOf0pt4])
# plt.xlim([10,200])
# extra_ticks = np.concatenate([np.arange(20,100,20),np.arange(100,200+1,100)],axis=1)
# plt.gca().get_xaxis().set_ticks(extra_ticks)
# plt.gca().get_xaxis().set_ticklabels(extra_ticks)
# plt.subplots_adjust(left=0.12, bottom=0.15, right=0.95, top=0.95,
#                     wspace=0.20, hspace=0.20)
# plt.ylim([10,70])
# extra_yticks = np.array([10,20,30,40,50,60,70])
# plt.gca().get_yaxis().set_ticks(extra_yticks)
# plt.gca().get_yaxis().set_ticklabels(extra_yticks)
# matplotlib.rcParams['xtick.labelsize'] = fontsize_ticks
# matplotlib.rcParams['ytick.labelsize'] = fontsize_ticks
# plt.show()


# plt.figure(figsize=(10,6))
# PlotMany_nstuck(A,[Filter_RadOf4pt0_ForceOf0pt4,
# Filter_RadOf4pt0_ForceOf0pt5,
# Filter_RadOf4pt0_ForceOf0pt75,
# Filter_RadOf4pt0_ForceOf1pt0,
# Filter_RadOf4pt0_ForceOf2pt0,
# Filter_RadOf4pt0_ForceOf3pt0
# ])
# extra_ticks = np.concatenate([np.arange(20,100,20),np.arange(100,400+1,100)],axis=1)
# plt.gca().get_xaxis().set_ticks(extra_ticks)
# plt.gca().get_xaxis().set_ticklabels(extra_ticks)
# plt.subplots_adjust(left=0.12, bottom=0.15, right=0.95, top=0.95,
#                     wspace=0.20, hspace=0.20)
# plt.show()


# plt.figure(figsize=(10,6))
# PlotMany_ntail(A,[Filter_RadOf4pt0_ForceOf0pt4])
# plt.xlim([0,200])
# extra_ticks = np.concatenate([np.arange(20,100,20),np.arange(100,200+1,100)],axis=1)
# plt.gca().get_xaxis().set_ticks(extra_ticks)
# plt.gca().get_xaxis().set_ticklabels(extra_ticks)
# plt.subplots_adjust(left=0.12, bottom=0.15, right=0.95, top=0.95,
#                     wspace=0.20, hspace=0.20)
# plt.ylim([0,160])
# plt.show()


# plt.figure(figsize=(10,6))
# PlotMany_ntail(A,[Filter_RadOf4pt0_ForceOf0pt4,
# Filter_RadOf4pt0_ForceOf0pt5,
# Filter_RadOf4pt0_ForceOf0pt75,
# Filter_RadOf4pt0_ForceOf1pt0,
# Filter_RadOf4pt0_ForceOf2pt0,
# Filter_RadOf4pt0_ForceOf3pt0
# ])
# extra_ticks = np.concatenate([np.arange(20,100,20),np.arange(100,400+1,100)],axis=1)
# plt.gca().get_xaxis().set_ticks(extra_ticks)
# plt.gca().get_xaxis().set_ticklabels(extra_ticks)
# plt.subplots_adjust(left=0.12, bottom=0.15, right=0.95, top=0.95,
#                     wspace=0.20, hspace=0.20)
# plt.show()


# plt.figure(figsize=(10,6))
# PlotMany_texit(A,[Filter_RadOf4pt0_ForceOf0pt4])
# plt.xlabel('')
# plt.text(46,3.3e1,'$N$',fontsize=fontsize_axislabels)
# plt.ylabel('')
# plt.text(8,4e2,'$t_{\mathrm{exit}}$',fontsize=fontsize_axislabels,rotation=90)
# plt.xlim([10,200])
# extra_ticks = np.concatenate([np.arange(20,100,20),np.arange(100,200+1,100)],axis=1)
# plt.gca().get_xaxis().set_ticks(extra_ticks)
# plt.gca().get_xaxis().set_ticklabels(extra_ticks)
# plt.ylim([5e1,6e3])
# #plt.gca().get_yaxis().set_ticks([2e3,4e3,1e4,2e4,4e4,1e5,2e5])
# #plt.gca().get_yaxis().set_ticklabels([r'$2\times 10^3$',r'$4\times 10^3$',r'$10^4$',
# #                                      r'$2\times 10^4$',r'$4\times 10^4$'
# #                                      ,r'$10^5$',r'$2\times 10^5$'])
# plt.subplots_adjust(left=0.10, bottom=0.12, right=0.95, top=0.98,
#                     wspace=0.20, hspace=0.20)
# plt.show()


# plt.figure(figsize=(10,6))
# PlotMany_texit(A,[Filter_RadOf4pt0_ForceOf0pt4,
# Filter_RadOf4pt0_ForceOf0pt5,
# Filter_RadOf4pt0_ForceOf0pt75,
# Filter_RadOf4pt0_ForceOf1pt0,
# Filter_RadOf4pt0_ForceOf2pt0,
# Filter_RadOf4pt0_ForceOf3pt0
# ])
# plt.xlabel('')
# plt.text(64,7,'$N$',fontsize=fontsize_axislabels)
# plt.ylabel('')
# plt.text(7,4e2,'$t_{\mathrm{exit}}$',fontsize=fontsize_axislabels,rotation=90)
# plt.xlim([10,400])
# extra_ticks = np.concatenate([np.arange(20,100,20),np.arange(100,400+1,100)],axis=1)
# plt.gca().get_xaxis().set_ticks(extra_ticks)
# plt.gca().get_xaxis().set_ticklabels(extra_ticks)
# plt.ylim([1.5e1,2e4])
# #plt.gca().get_yaxis().set_ticks([2e3,4e3,1e4,2e4,4e4,1e5,2e5])
# #plt.gca().get_yaxis().set_ticklabels([r'$2\times 10^3$',r'$4\times 10^3$',r'$10^4$',
# #                                      r'$2\times 10^4$',r'$4\times 10^4$'
# #                                      ,r'$10^5$',r'$2\times 10^5$'])
# plt.subplots_adjust(left=0.10, bottom=0.12, right=0.95, top=0.98,
#                     wspace=0.20, hspace=0.20)
# plt.show()


# plt.figure(figsize=(10,6))
# PlotMany_nexit(A,[Filter_RadOf4pt0_ForceOf0pt4])
# plt.xlim([10,200])
# plt.ylim([5,35])
# extra_ticks = np.concatenate([np.arange(20,100,20),np.arange(100,200+1,100)],axis=1)
# plt.gca().get_xaxis().set_ticks(extra_ticks)
# plt.gca().get_xaxis().set_ticklabels(extra_ticks)
# yticks = [5,10,15,20,25,30,35]
# plt.gca().get_yaxis().set_ticks(yticks)
# plt.gca().get_yaxis().set_ticklabels(yticks)
# plt.subplots_adjust(left=0.12, bottom=0.15, right=0.95, top=0.95,
#                     wspace=0.20, hspace=0.20)
# plt.show()


# plt.figure(figsize=(10,6))
# PlotMany_nexit(A,[Filter_RadOf4pt0_ForceOf0pt4,
# Filter_RadOf4pt0_ForceOf0pt5,
# Filter_RadOf4pt0_ForceOf0pt75,
# Filter_RadOf4pt0_ForceOf1pt0,
# Filter_RadOf4pt0_ForceOf2pt0,
# Filter_RadOf4pt0_ForceOf3pt0
# ])
# plt.xlim([10,400])
# plt.ylim([5,80])
# extra_ticks = np.concatenate([np.arange(20,100,20),np.arange(100,400+1,100)],axis=1)
# plt.gca().get_xaxis().set_ticks(extra_ticks)
# plt.gca().get_xaxis().set_ticklabels(extra_ticks)
# yticks = [10,20,30,40,50,60,70,80]
# plt.gca().get_yaxis().set_ticks(yticks)
# plt.gca().get_yaxis().set_ticklabels(yticks)
# plt.subplots_adjust(left=0.12, bottom=0.15, right=0.95, top=0.95,
#                     wspace=0.20, hspace=0.20)
# plt.show()


# plt.figure(figsize=(10,6))
# PlotMany_failrate(A,[Filter_RadOf4pt0_ForceOf0pt4,
# Filter_RadOf4pt0_ForceOf0pt5,
# Filter_RadOf4pt0_ForceOf0pt75,
# Filter_RadOf4pt0_ForceOf1pt0,
# Filter_RadOf4pt0_ForceOf2pt0,
# Filter_RadOf4pt0_ForceOf3pt0
# ])
# plt.subplots_adjust(left=0.12, bottom=0.15, right=0.95, top=0.95,
#                     wspace=0.20, hspace=0.20)
# plt.show()


# plt.figure(figsize=(10,6))
# PlotMany_NcritComp(A,[Filter_RadOf3pt0_ForceOf0pt4,
# Filter_RadOf3pt0_ForceOf0pt5,
# Filter_RadOf3pt0_ForceOf0pt6,
# Filter_RadOf3pt0_ForceOf0pt75,
# Filter_RadOf3pt0_ForceOf1pt0,
# Filter_RadOf3pt5_ForceOf0pt4,
# Filter_RadOf3pt5_ForceOf0pt5,
# Filter_RadOf3pt5_ForceOf0pt6,
# Filter_RadOf3pt5_ForceOf0pt75,
# Filter_RadOf3pt5_ForceOf1pt0,
# Filter_RadOf4pt0_ForceOf0pt4,
# Filter_RadOf4pt0_ForceOf0pt5,
# Filter_RadOf4pt0_ForceOf0pt6,
# Filter_RadOf4pt0_ForceOf0pt75,
# Filter_RadOf4pt0_ForceOf1pt0
# ])
# plt.subplots_adjust(left=0.12, bottom=0.15, right=0.95, top=0.95,
#                     wspace=0.20, hspace=0.20)
# plt.show()


############## Rnom 15##############################


# plt.figure(figsize=(10,6))
# PlotMany_ttrans(B,[FilterB_RadOf4pt0_ForceOf0pt4])
# plt.xlabel('')
# plt.text(46,6.5e3,'$N$',fontsize=fontsize_axislabels)
# plt.ylabel('')
# plt.text(7,1.4e4,'$t_{\mathrm{trans}}$',fontsize=fontsize_axislabels,rotation=90)
# plt.xlim([10,200])
# extra_ticks = np.concatenate([np.arange(20,100,20),np.arange(100,200+1,100)],axis=1)
# plt.gca().get_xaxis().set_ticks(extra_ticks)
# plt.gca().get_xaxis().set_ticklabels(extra_ticks)
# plt.ylim([7.5e3,3.5e4])
# plt.gca().get_yaxis().set_ticks([8e3,9e3,1e4,2e4,3e4])
# plt.gca().get_yaxis().set_ticklabels([r'$8\times 10^3$',r'$9\times 10^3$'
#                                       ,r'$10^4$',r'$2\times 10^4$',r'$3\times 10^4$'])
# plt.subplots_adjust(left=0.12, bottom=0.10, right=0.95, top=0.98,
#                     wspace=0.20, hspace=0.20)
# plt.show()


# plt.figure(figsize=(10,6))
# PlotMany_ttrans(B,[FilterB_RadOf4pt0_ForceOf0pt4])
# PlotMany_tstuck(B,[FilterB_RadOf4pt0_ForceOf0pt4])
# PlotMany_texit(B,[FilterB_RadOf4pt0_ForceOf0pt4])
# plt.xlabel('')
# plt.text(46,6.5e3,'$N$',fontsize=fontsize_axislabels)
# plt.ylabel('')
# plt.text(7,1.4e4,'$t_{\mathrm{trans}}$',fontsize=fontsize_axislabels,rotation=90)
# plt.xlim([10,200])
# extra_ticks = np.concatenate([np.arange(20,100,20),np.arange(100,200+1,100)],axis=1)
# plt.gca().get_xaxis().set_ticks(extra_ticks)
# plt.gca().get_xaxis().set_ticklabels(extra_ticks)
# # plt.ylim([7.5e3,3.5e4])
# # plt.gca().get_yaxis().set_ticks([8e3,9e3,1e4,2e4,3e4])
# # plt.gca().get_yaxis().set_ticklabels([r'$8\times 10^3$',r'$9\times 10^3$'
# #                                       ,r'$10^4$',r'$2\times 10^4$',r'$3\times 10^4$'])
# plt.subplots_adjust(left=0.12, bottom=0.10, right=0.95, top=0.98,
#                     wspace=0.20, hspace=0.20)
# plt.show()


# plt.figure(figsize=(10,6))
# PlotMany_ttrans(B,[FilterB_RadOf4pt0_ForceOf1pt0])
# plt.xlabel('')
# plt.text(46,1.65e3,'$N$',fontsize=fontsize_axislabels)
# plt.ylabel('')
# plt.text(7,1.4e4,'$t_{\mathrm{trans}}$',fontsize=fontsize_axislabels,rotation=90)
# plt.xlim([10,400])
# extra_ticks = np.concatenate([np.arange(20,100,20),np.arange(100,400+1,100)],axis=1)
# plt.gca().get_xaxis().set_ticks(extra_ticks)
# plt.gca().get_xaxis().set_ticklabels(extra_ticks)
# plt.ylim([2.5e3,2e4])
# plt.gca().get_yaxis().set_ticks([2e3,4e3,6e3,8e3,1e4])
# plt.gca().get_yaxis().set_ticklabels([r'$2\times 10^3$',r'$4\times 10^3$',r'$6\times 10^3$',
#                                       r'$8\times 10^3$',r'$10^4$'])
# plt.subplots_adjust(left=0.12, bottom=0.10, right=0.95, top=0.98,
#                     wspace=0.20, hspace=0.20)
# plt.show()


# plt.figure(figsize=(10,6))
# PlotMany_ttrans(B,[FilterB_RadOf4pt0_ForceOf0pt4,
# FilterB_RadOf4pt0_ForceOf0pt5,
# FilterB_RadOf4pt0_ForceOf0pt75,
# FilterB_RadOf4pt0_ForceOf1pt0,
# FilterB_RadOf4pt0_ForceOf1pt5,
# ])
# plt.xlabel('')
# plt.text(46,1.e3,'$N$',fontsize=fontsize_axislabels)
# plt.ylabel('')
# plt.text(7,9e3,'$t_{\mathrm{trans}}$',fontsize=fontsize_axislabels,rotation=90)
# plt.xlim([10,400])
# extra_ticks = np.concatenate([np.arange(20,100,20),np.arange(100,400+1,100)],axis=1)
# plt.gca().get_xaxis().set_ticks(extra_ticks)
# plt.gca().get_xaxis().set_ticklabels(extra_ticks)
# plt.ylim([1.3e3,3.5e4])
# plt.gca().get_yaxis().set_ticks([2e3,4e3,1e4,2e4])
# plt.gca().get_yaxis().set_ticklabels([r'$2\times 10^3$',r'$4\times 10^3$',r'$10^4$',
#                                       r'$2\times 10^4$'])
# plt.subplots_adjust(left=0.10, bottom=0.10, right=0.95, top=0.98,
#                     wspace=0.20, hspace=0.20)
# plt.show()


# plt.figure(figsize=(10,6))
# PlotMany_tstuck(B,[FilterB_RadOf4pt0_ForceOf0pt4])
# plt.xlabel('')
# plt.text(46,6.5e3,'$N$',fontsize=fontsize_axislabels)
# plt.ylabel('')
# plt.text(7,1.4e4,'$t_{\mathrm{stuck}}$',fontsize=fontsize_axislabels,rotation=90)
# plt.xlim([10,200])
# extra_ticks = np.concatenate([np.arange(20,100,20),np.arange(100,200+1,100)],axis=1)
# plt.gca().get_xaxis().set_ticks(extra_ticks)
# plt.gca().get_xaxis().set_ticklabels(extra_ticks)
# plt.ylim([7.5e3,3.5e4])
# plt.gca().get_yaxis().set_ticks([8e3,9e3,1e4,2e4,3e4])
# plt.gca().get_yaxis().set_ticklabels([r'$8\times 10^3$',r'$9\times 10^3$'
#                                       ,r'$10^4$',r'$2\times 10^4$',r'$3\times 10^4$'])
# plt.subplots_adjust(left=0.12, bottom=0.10, right=0.95, top=0.98,
#                     wspace=0.20, hspace=0.20)
# plt.show()


# plt.figure(figsize=(10,6))
# PlotMany_tstuck(B,[FilterB_RadOf4pt0_ForceOf0pt4,
# FilterB_RadOf4pt0_ForceOf0pt5,
# FilterB_RadOf4pt0_ForceOf0pt75,
# FilterB_RadOf4pt0_ForceOf1pt0,
# FilterB_RadOf4pt0_ForceOf1pt5,
# ])
# plt.xlabel('')
# plt.text(46,7.5e2,'$N$',fontsize=fontsize_axislabels)
# plt.ylabel('')
# plt.text(7,9e3,'$t_{\mathrm{stuck}}$',fontsize=fontsize_axislabels,rotation=90)
# plt.xlim([10,400])
# extra_ticks = np.concatenate([np.arange(20,100,20),np.arange(100,400+1,100)],axis=1)
# plt.gca().get_xaxis().set_ticks(extra_ticks)
# plt.gca().get_xaxis().set_ticklabels(extra_ticks)
# plt.ylim([1.e3,3.5e4])
# plt.gca().get_yaxis().set_ticks([1e3,2e3,4e3,1e4,2e4])
# plt.gca().get_yaxis().set_ticklabels([r'$10^3$',r'$2\times 10^3$',r'$4\times 10^3$',r'$10^4$',
#                                       r'$2\times 10^4$'])
# plt.subplots_adjust(left=0.10, bottom=0.10, right=0.95, top=0.98,
#                     wspace=0.20, hspace=0.20)
# plt.show()


# plt.figure(figsize=(10,6))
# PlotMany_nstuck(B,[FilterB_RadOf4pt0_ForceOf0pt4,
# FilterB_RadOf4pt0_ForceOf0pt5,
# FilterB_RadOf4pt0_ForceOf0pt75,
# FilterB_RadOf4pt0_ForceOf1pt0,
# FilterB_RadOf4pt0_ForceOf1pt5,
# ])
# plt.xlim([10,400])
# plt.ylim([10,110])
# extra_ticks = np.concatenate([np.arange(20,100,20),np.arange(100,400+1,100)],axis=1)
# plt.gca().get_xaxis().set_ticks(extra_ticks)
# plt.gca().get_xaxis().set_ticklabels(extra_ticks)
# yticks = [10,30,50,70,90,110]
# plt.gca().get_yaxis().set_ticks(yticks)
# plt.gca().get_yaxis().set_ticklabels(yticks)
# plt.subplots_adjust(left=0.12, bottom=0.15, right=0.95, top=0.95,
#                     wspace=0.20, hspace=0.20)
# plt.show()


# plt.figure(figsize=(10,6))
# PlotMany_ntail(B,[FilterB_RadOf4pt0_ForceOf0pt4,
# FilterB_RadOf4pt0_ForceOf0pt5,
# FilterB_RadOf4pt0_ForceOf0pt75,
# FilterB_RadOf4pt0_ForceOf1pt0,
# FilterB_RadOf4pt0_ForceOf1pt5,
# ])
# extra_ticks = np.concatenate([np.arange(20,100,20),np.arange(100,400+1,100)],axis=1)
# plt.gca().get_xaxis().set_ticks(extra_ticks)
# plt.gca().get_xaxis().set_ticklabels(extra_ticks)
# plt.subplots_adjust(left=0.12, bottom=0.15, right=0.95, top=0.95,
#                     wspace=0.20, hspace=0.20)
# plt.show()


# plt.figure(figsize=(10,6))
# PlotMany_tstuckVnstuck(B,[FilterB_RadOf4pt0_ForceOf0pt4,
# FilterB_RadOf4pt0_ForceOf0pt5,
# FilterB_RadOf4pt0_ForceOf0pt75,
# FilterB_RadOf4pt0_ForceOf1pt0,
# FilterB_RadOf4pt0_ForceOf1pt5,
# ])
# plt.xlabel('')
# plt.text(25,7.5e2,'$N_{\mathrm{stuck}}$',fontsize=fontsize_axislabels)
# plt.ylabel('')
# plt.text(7.5,1e4,'$t_{\mathrm{stuck}}$',fontsize=fontsize_axislabels,rotation=90)
# plt.xlim([10,100])
# extra_ticks = np.concatenate([np.arange(20,100,20),np.arange(100,100+1,100)],axis=1)
# plt.gca().get_xaxis().set_ticks(extra_ticks)
# plt.gca().get_xaxis().set_ticklabels(extra_ticks)
# plt.ylim([1.e3,2.8e4])
# plt.gca().get_yaxis().set_ticks([2e3,4e3,1e4,2e4])
# plt.gca().get_yaxis().set_ticklabels([r'$2\times 10^3$',r'$4\times 10^3$',r'$10^4$',
#                                       r'$2\times 10^4$'])
# plt.subplots_adjust(left=0.12, bottom=0.12, right=0.95, top=0.98,
#                     wspace=0.20, hspace=0.20)
# plt.show()


# plt.figure(figsize=(10,6))
# PlotMany_tstuckVnstuck(B,[FilterB_RadOf4pt0_ForceOf0pt4,
# FilterB_RadOf4pt0_ForceOf0pt5,
# FilterB_RadOf4pt0_ForceOf0pt75,
# ])
# plt.xlabel('')
# plt.text(25,2.e3,'$N_{\mathrm{stuck}}$',fontsize=fontsize_axislabels)
# plt.ylabel('')
# plt.text(7.5,1.3e4,'$t_{\mathrm{stuck}}$',fontsize=fontsize_axislabels,rotation=90)
# plt.xlim([10,100])
# extra_ticks = np.concatenate([np.arange(20,100,20),np.arange(100,100+1,100)],axis=1)
# plt.gca().get_xaxis().set_ticks(extra_ticks)
# plt.gca().get_xaxis().set_ticklabels(extra_ticks)
# plt.ylim([2.5e3,3.5e4])
# plt.gca().get_yaxis().set_ticks([4e3,6e3,8e3,1e4,2e4])
# plt.gca().get_yaxis().set_ticklabels([r'$4\times 10^3$',r'$6\times 10^3$',r'$8\times 10^3$',
#                                       r'$10^4$',
#                                       r'$2\times 10^4$'])
# plt.subplots_adjust(left=0.12, bottom=0.12, right=0.95, top=0.98,
#                     wspace=0.20, hspace=0.20)
# plt.show()


# plt.figure(figsize=(10,6))
# PlotMany_texit(B,[FilterB_RadOf4pt0_ForceOf0pt4,
# FilterB_RadOf4pt0_ForceOf0pt5,
# FilterB_RadOf4pt0_ForceOf0pt75,
# FilterB_RadOf4pt0_ForceOf1pt0,
# FilterB_RadOf4pt0_ForceOf1pt5,
# ])
# plt.xlabel('')
# plt.text(46,8.e0,'$N$',fontsize=fontsize_axislabels)
# plt.ylabel('')
# plt.text(7,4e2,'$t_{\mathrm{exit}}$',fontsize=fontsize_axislabels,rotation=90)
# plt.xlim([10,400])
# extra_ticks = np.concatenate([np.arange(20,100,20),np.arange(100,400+1,100)],axis=1)
# plt.gca().get_xaxis().set_ticks(extra_ticks)
# plt.gca().get_xaxis().set_ticklabels(extra_ticks)
# plt.ylim([2.5e1,1.5e4])
# #plt.gca().get_yaxis().set_ticks([1e3,2e3,4e3,1e4,2e4])
# #plt.gca().get_yaxis().set_ticklabels([r'$10^3$',r'$2\times 10^3$',r'$4\times 10^3$',r'$10^4$',
# #                                      r'$2\times 10^4$'])
# plt.subplots_adjust(left=0.10, bottom=0.10, right=0.95, top=0.98,
#                     wspace=0.20, hspace=0.20)
# plt.show()


# plt.figure(figsize=(10,6))
# PlotMany_nexit(B,[FilterB_RadOf4pt0_ForceOf0pt4,
# FilterB_RadOf4pt0_ForceOf0pt5,
# FilterB_RadOf4pt0_ForceOf0pt75,
# FilterB_RadOf4pt0_ForceOf1pt0,
# FilterB_RadOf4pt0_ForceOf1pt5,
# ])
# plt.xlim([10,400])
# plt.ylim([5,60])
# extra_ticks = np.concatenate([np.arange(20,100,20),np.arange(100,400+1,100)],axis=1)
# plt.gca().get_xaxis().set_ticks(extra_ticks)
# plt.gca().get_xaxis().set_ticklabels(extra_ticks)
# yticks = [10,20,30,40,50,60]
# plt.gca().get_yaxis().set_ticks(yticks)
# plt.gca().get_yaxis().set_ticklabels(yticks)
# plt.subplots_adjust(left=0.12, bottom=0.15, right=0.95, top=0.95,
#                     wspace=0.20, hspace=0.20)
# plt.show()


plt.figure(figsize=(10,6))
PlotMany_NcritComp(B,[FilterB_RadOf3pt0_ForceOf0pt4,
FilterB_RadOf3pt0_ForceOf0pt5,
FilterB_RadOf3pt0_ForceOf0pt6,
FilterB_RadOf3pt0_ForceOf0pt75,
FilterB_RadOf3pt0_ForceOf1pt0,
FilterB_RadOf3pt5_ForceOf0pt4,
FilterB_RadOf3pt5_ForceOf0pt5,
FilterB_RadOf3pt5_ForceOf0pt6,
FilterB_RadOf3pt5_ForceOf0pt75,
FilterB_RadOf3pt5_ForceOf1pt0,
FilterB_RadOf4pt0_ForceOf0pt4,
FilterB_RadOf4pt0_ForceOf0pt5,
FilterB_RadOf4pt0_ForceOf0pt6,
FilterB_RadOf4pt0_ForceOf0pt75,
FilterB_RadOf4pt0_ForceOf1pt0
])
plt.subplots_adjust(left=0.12, bottom=0.15, right=0.95, top=0.95,
                    wspace=0.20, hspace=0.20)
plt.show()


############## Mixed ##############################


# plt.figure(figsize=(10,6))
# PlotMany_ttrans(A,[Filter_RadOf4pt0_ForceOf0pt5])
# PlotMany_ttrans(B,[FilterB_RadOf4pt0_ForceOf0pt5])
# plt.xlabel('')
# plt.text(46,4.5e3,'$N$',fontsize=fontsize_axislabels)
# plt.ylabel('')
# plt.text(8,5e4,'$t_{\mathrm{trans}}$',fontsize=fontsize_axislabels,rotation=90)
# plt.xlim([10,200])
# extra_ticks = np.concatenate([np.arange(20,100,20),np.arange(100,200+1,100)],axis=1)
# plt.gca().get_xaxis().set_ticks(extra_ticks)
# plt.gca().get_xaxis().set_ticklabels(extra_ticks)
# plt.ylim([6e3,1.7e5])
# plt.text(30,1.1e5,'$r_{\mathrm{nom,pore}} = 1.3\sigma$',fontsize=fontsize_innerlabels,color='b')
# plt.text(13,2e4,'$r_{\mathrm{nom,pore}} = 1.5\sigma$',fontsize=fontsize_innerlabels,color='g')
# plt.subplots_adjust(left=0.12, bottom=0.10, right=0.95, top=0.98,
#                     wspace=0.20, hspace=0.20)
# plt.show()


# plt.figure(figsize=(10,6))
# PlotMany_tstuck(A,[Filter_RadOf4pt0_ForceOf0pt5])
# PlotMany_tstuck(B,[FilterB_RadOf4pt0_ForceOf0pt5])
# plt.xlabel('')
# plt.text(46,3.e3,'$N$',fontsize=fontsize_axislabels)
# plt.ylabel('')
# plt.text(8,5e4,'$t_{\mathrm{stuck}}$',fontsize=fontsize_axislabels,rotation=90)
# plt.xlim([10,200])
# extra_ticks = np.concatenate([np.arange(20,100,20),np.arange(100,200+1,100)],axis=1)
# plt.gca().get_xaxis().set_ticks(extra_ticks)
# plt.gca().get_xaxis().set_ticklabels(extra_ticks)
# plt.ylim([4e3,1.7e5])
# plt.text(30,1.1e5,'$r_{\mathrm{nom,pore}} = 1.3\sigma$',fontsize=fontsize_innerlabels,color='b')
# plt.text(13,1.75e4,'$r_{\mathrm{nom,pore}} = 1.5\sigma$',fontsize=fontsize_innerlabels,color='g')
# plt.subplots_adjust(left=0.12, bottom=0.10, right=0.95, top=0.98,
#                     wspace=0.20, hspace=0.20)
# plt.show()


# plt.figure(figsize=(10,6))
# PlotMany_timedivvy(B,[FilterB_RadOf4pt0_ForceOf0pt5])
# plt.xlabel('')
# plt.text(46,4.5e3,'$N$',fontsize=fontsize_axislabels)
# plt.ylabel('')
# plt.text(8,1.2e4,'Time',fontsize=fontsize_axislabels,rotation=90)
# plt.xlim([10,200])
# extra_ticks = np.concatenate([np.arange(20,100,20),np.arange(100,200+1,100)],axis=1)
# plt.gca().get_xaxis().set_ticks(extra_ticks)
# plt.gca().get_xaxis().set_ticklabels(extra_ticks)
# plt.ylim([5e3,1.5e4])
# plt.gca().get_yaxis().set_ticks([5e3,6e3,7e3,8e3,9e3,1e4])
# plt.gca().get_yaxis().set_ticklabels([r'$5\times 10^3$',r'$6\times 10^3$',
#                                       r'$7\times 10^3$',r'$8\times 10^3$',r'$9\times 10^3$',
#                                       r'$10^4$'])
# plt.text(100,1.3e4,'$t_{\mathrm{trans}}$',fontsize=fontsize_innerlabels,color='b')
# plt.text(120,1.15e4,'$t_{\mathrm{stuck}}+t_{\mathrm{exit}}$',fontsize=fontsize_innerlabels,color='r',rotation=30)
# plt.text(130,8.5e3,'$t_{\mathrm{stuck}}$',fontsize=fontsize_innerlabels,color='g')
# plt.subplots_adjust(left=0.12, bottom=0.10, right=0.95, top=0.95,
#                     wspace=0.20, hspace=0.20)
# plt.show()


# plt.figure(figsize=(10,6))
# PlotMany_ntail(A,[Filter_RadOf4pt0_ForceOf0pt5])
# PlotMany_ntail(B,[FilterB_RadOf4pt0_ForceOf0pt5])
# plt.xlim([0,200])
# plt.ylim([0,160])
# extra_ticks = np.concatenate([np.arange(20,100,20),np.arange(100,200+1,100)],axis=1)
# plt.gca().get_xaxis().set_ticks(extra_ticks)
# plt.gca().get_xaxis().set_ticklabels(extra_ticks)
# plt.subplots_adjust(left=0.12, bottom=0.15, right=0.95, top=0.95,
#                     wspace=0.20, hspace=0.20)
# opt = dict(color='k',width=0.5)
# plt.annotate("$r_{\mathrm{nom,pore}}=1.3\sigma$", xytext=(100,15), xy=(85,8), fontsize=fontsize_innerlabels,arrowprops=opt,color='b')
# plt.annotate("$r_{\mathrm{nom,pore}}=1.5\sigma$", xytext=(30,15), xy=(75,10), fontsize=fontsize_innerlabels,arrowprops=opt,color='g')
# plt.show()


