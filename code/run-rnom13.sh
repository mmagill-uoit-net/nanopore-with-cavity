#!/bin/bash


# Run rnom13 at high forces
# Reduced number of cases for faster turnaround

# Constant parameters
rseed_suffix=123456
vis_flag=0
nevents=50

# Make log directory if it isn't already theree
mkdir -p logs

# Should tailor time limit to each set of cases... but not for now
time=5000m

for rseed_prefix in {11..15} {21..25}
do

   # ############################ Small Cavity
   # reff_cavi=3.

   # force_pore=0.4
   # for N in 33 36 39 42 45
   # do
   #    rseed=${N}${rseed_prefix}${rseed_suffix}
   #    label=${reff_cavi}_${force_pore}_${N}_${rseed}
   #    sqsub -q serial -o logs/log_${label}.log -r $time ~/espresso-2.1.2j/Espresso  nanopore-with-cavity-rnom13.tcl $N $reff_cavi $force_pore $vis_flag $nevents $rseed
   # done

   ############################ Large Cavity
   reff_cavi=4.

   # force_pore=0.4
   # for N in 80 85 90 95
   # do
   #    rseed=${N}${rseed_prefix}${rseed_suffix}
   #    label=${reff_cavi}_${force_pore}_${N}_${rseed}
   #    sqsub -q serial -o logs/log_${label}.log -r $time ~/espresso-2.1.2j/Espresso  nanopore-with-cavity-rnom13.tcl $N $reff_cavi $force_pore $vis_flag $nevents $rseed
   # done

   # force_pore=1.6
   # for N in 10 30 50 75 100 125 150 200 300 400
   # do
   #    rseed=${N}${rseed_prefix}${rseed_suffix}
   #    label=${reff_cavi}_${force_pore}_${N}_${rseed}
   #    sqsub -q serial -o logs/log_${label}.log -r $time ~/espresso-2.1.2j/Espresso  nanopore-with-cavity-rnom13.tcl $N $reff_cavi $force_pore $vis_flag $nevents $rseed
   # done

   # force_pore=1.7
   # for N in 10 30 50 75 100 125 150 200 300 400
   # do
   #    rseed=${N}${rseed_prefix}${rseed_suffix}
   #    label=${reff_cavi}_${force_pore}_${N}_${rseed}
   #    sqsub -q serial -o logs/log_${label}.log -r $time ~/espresso-2.1.2j/Espresso  nanopore-with-cavity-rnom13.tcl $N $reff_cavi $force_pore $vis_flag $nevents $rseed
   # done

   # force_pore=1.8
   # for N in 10 30 50 75 100 125 150 200 300 400
   # do
   #    rseed=${N}${rseed_prefix}${rseed_suffix}
   #    label=${reff_cavi}_${force_pore}_${N}_${rseed}
   #    sqsub -q serial -o logs/log_${label}.log -r $time ~/espresso-2.1.2j/Espresso  nanopore-with-cavity-rnom13.tcl $N $reff_cavi $force_pore $vis_flag $nevents $rseed
   # done

   # force_pore=1.9
   # for N in 10 30 50 75 100 125 150 200 300 400
   # do
   #    rseed=${N}${rseed_prefix}${rseed_suffix}
   #    label=${reff_cavi}_${force_pore}_${N}_${rseed}
   #    sqsub -q serial -o logs/log_${label}.log -r $time ~/espresso-2.1.2j/Espresso  nanopore-with-cavity-rnom13.tcl $N $reff_cavi $force_pore $vis_flag $nevents $rseed
   # done

   # force_pore=2.0
   # for N in 10 30 50 75 100 125 150 200 300 400
   # do
   #    rseed=${N}${rseed_prefix}${rseed_suffix}
   #    label=${reff_cavi}_${force_pore}_${N}_${rseed}
   #    sqsub -q serial -o logs/log_${label}.log -r $time ~/espresso-2.1.2j/Espresso  nanopore-with-cavity-rnom13.tcl $N $reff_cavi $force_pore $vis_flag $nevents $rseed
   # done

   force_pore=3.0
#   for N in 10 30 50 75 100 125 150 200 300 400
   for N in 175 225 250
   do
      rseed=${N}${rseed_prefix}${rseed_suffix}
      label=${reff_cavi}_${force_pore}_${N}_${rseed}
      sqsub -q serial -o logs/log_${label}.log -r $time ~/espresso-2.1.2j/Espresso  nanopore-with-cavity-rnom13.tcl $N $reff_cavi $force_pore $vis_flag $nevents $rseed
   done

   # force_pore=4.0
   # for N in 10 30 50 75 100 125 150 200 300 400
   # do
   #    rseed=${N}${rseed_prefix}${rseed_suffix}
   #    label=${reff_cavi}_${force_pore}_${N}_${rseed}
   #    sqsub -q serial -o logs/log_${label}.log -r $time ~/espresso-2.1.2j/Espresso  nanopore-with-cavity-rnom13.tcl $N $reff_cavi $force_pore $vis_flag $nevents $rseed
   # done


done






