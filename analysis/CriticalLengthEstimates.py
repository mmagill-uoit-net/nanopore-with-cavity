import numpy as np
import matplotlib.pyplot as plt
import matplotlib
from scipy.optimize import fsolve
from matplotlib.ticker import MultipleLocator, FormatStrFormatter

# Use latex
plt.rc('text', usetex=True)


fig, ax = plt.subplots(figsize=(16,12))
textsize=32

# Function definitions #

def CalcNfree_RG(r):
	return (np.sqrt(6) * (r))**(1./0.59)

def CalcNfree_RF(r):
	return (np.sqrt(0.952) * (r))**(1./0.59)

def CalcNpacked_max(r):
	packing_factor = 0.75	# Kepler hypothesis
	return packing_factor*(12.*r**3)

def CalcNpacked_rand(r):
	packing_factor = 0.64	# Random packing
	return packing_factor*(12.*r**3)

def CalcNcrit(r,Fpore):
#	B = 5.5
#	return ( (Fpore/(2.31*B)) * (np.sqrt(6)*r)**3.93 )**(1./1.31)
#        nu = 0.588
#        CG = 0.408
#        B = 5.5
        nu = 0.588
        CG = 0.402
        B = 5.9
	return  ( ((3*nu-1)/(3*nu)) * (Fpore/B) )**(3*nu-1) * (r/CG)**3.


# End of functions #


## Plotting parameters
rmin = 2
rmax = 10

## Plot data (currently defined as length of minimal t_stuck)

matplotlib.rcParams['lines.markersize'] = 12
matplotlib.rcParams['lines.markeredgewidth'] = 3

# #### rnom=1.3 ttrans
# ax.plot(3,30,'o',markeredgecolor='b',markerfacecolor='none')
# ax.plot(3,34,'^',markeredgecolor='g',markerfacecolor='none')
# ax.plot(3,37,'*',markeredgecolor='y',markerfacecolor='none')
# ax.plot(3,40,'s',markeredgecolor='r',markerfacecolor='none')
# ax.plot(3,47,'d',markeredgecolor='c',markerfacecolor='none')
# ax.plot(3,50,'x',markeredgecolor='m',markerfacecolor='none')

# ax.plot(3.5,45,'o',markeredgecolor='b',markerfacecolor='none')
# ax.plot(3.5,55,'^',markeredgecolor='g',markerfacecolor='none')
# ax.plot(3.5,58,'*',markeredgecolor='y',markerfacecolor='none')
# ax.plot(3.5,65,'s',markeredgecolor='r',markerfacecolor='none')
# ax.plot(3.5,80,'d',markeredgecolor='c',markerfacecolor='none')
# ax.plot(3.5,90,'x',markeredgecolor='m',markerfacecolor='none')

# ax.plot(4,70,'o',markeredgecolor='b',markerfacecolor='none')
# ax.plot(4,80,'^',markeredgecolor='g',markerfacecolor='none')
# ax.plot(4,87,'*',markeredgecolor='y',markerfacecolor='none')
# ax.plot(4,100,'s',markeredgecolor='r',markerfacecolor='none')
# ax.plot(4,115,'d',markeredgecolor='c',markerfacecolor='none')
# ax.plot(4,140,'x',markeredgecolor='m',markerfacecolor='none')
# ####

#### rnom=1.3 tstuck
ax.plot(3,30,'o',markeredgecolor='b',markerfacecolor='none')
ax.plot(3,34,'^',markeredgecolor='g',markerfacecolor='none')
ax.plot(3,37,'*',markeredgecolor='y',markerfacecolor='none')
ax.plot(3,40,'s',markeredgecolor='r',markerfacecolor='none')
ax.plot(3,47,'d',markeredgecolor='c',markerfacecolor='none')
ax.plot(3,50,'x',markeredgecolor='m',markerfacecolor='none')

ax.plot(3.5,45,'o',markeredgecolor='b',markerfacecolor='none')
ax.plot(3.5,55,'^',markeredgecolor='g',markerfacecolor='none')
ax.plot(3.5,60,'*',markeredgecolor='y',markerfacecolor='none')
ax.plot(3.5,65,'s',markeredgecolor='r',markerfacecolor='none')
ax.plot(3.5,80,'d',markeredgecolor='c',markerfacecolor='none')
ax.plot(3.5,90,'x',markeredgecolor='m',markerfacecolor='none')

ax.plot(4,75,'o',markeredgecolor='b',markerfacecolor='none')
ax.plot(4,80,'^',markeredgecolor='g',markerfacecolor='none')
ax.plot(4,87,'*',markeredgecolor='y',markerfacecolor='none')
ax.plot(4,100,'s',markeredgecolor='r',markerfacecolor='none')
ax.plot(4,115,'d',markeredgecolor='c',markerfacecolor='none')
ax.plot(4,140,'x',markeredgecolor='m',markerfacecolor='none')
####

# #### rnom=1.3 nstuck
# ax.plot(3,30,'o',markeredgecolor='b',markerfacecolor='none')
# ax.plot(3,34,'^',markeredgecolor='g',markerfacecolor='none')
# ax.plot(3,37,'*',markeredgecolor='y',markerfacecolor='none')
# ax.plot(3,40,'s',markeredgecolor='r',markerfacecolor='none')
# ax.plot(3,50,'d',markeredgecolor='c',markerfacecolor='none')
# ax.plot(3,65,'x',markeredgecolor='m',markerfacecolor='none')

# ax.plot(3.5,45,'o',markeredgecolor='b',markerfacecolor='none')
# ax.plot(3.5,55,'^',markeredgecolor='g',markerfacecolor='none')
# ax.plot(3.5,60,'*',markeredgecolor='y',markerfacecolor='none')
# ax.plot(3.5,70,'s',markeredgecolor='r',markerfacecolor='none')
# ax.plot(3.5,80,'d',markeredgecolor='c',markerfacecolor='none')
# ax.plot(3.5,110,'x',markeredgecolor='m',markerfacecolor='none')

# ax.plot(4,70,'o',markeredgecolor='b',markerfacecolor='none')
# ax.plot(4,80,'^',markeredgecolor='g',markerfacecolor='none')
# ax.plot(4,90,'*',markeredgecolor='y',markerfacecolor='none')
# ax.plot(4,105,'s',markeredgecolor='r',markerfacecolor='none')
# ax.plot(4,125,'d',markeredgecolor='c',markerfacecolor='none')
# ax.plot(4,160,'x',markeredgecolor='m',markerfacecolor='none')
# ####

# #### rnom=1.5 ttrans
# ax.plot(3,25,'o',markeredgecolor='b',markerfacecolor='none')
# ax.plot(3,28,'^',markeredgecolor='g',markerfacecolor='none')
# ax.plot(3,25,'s',markeredgecolor='r',markerfacecolor='none')
# ax.plot(3,15,'d',markeredgecolor='c',markerfacecolor='none')
# ax.plot(3,10,'x',markeredgecolor='m',markerfacecolor='none')

# ax.plot(3.5,40,'o',markeredgecolor='b',markerfacecolor='none')
# ax.plot(3.5,45,'^',markeredgecolor='g',markerfacecolor='none')
# ax.plot(3.5,50,'s',markeredgecolor='r',markerfacecolor='none')
# ax.plot(3.5,60,'d',markeredgecolor='c',markerfacecolor='none')
# ax.plot(3.5,10,'x',markeredgecolor='m',markerfacecolor='none')

# ax.plot(4,65,'o',markeredgecolor='b',markerfacecolor='none')
# ax.plot(4,75,'^',markeredgecolor='g',markerfacecolor='none')
# ax.plot(4,80,'s',markeredgecolor='r',markerfacecolor='none')
# ax.plot(4,95,'d',markeredgecolor='c',markerfacecolor='none')
# ax.plot(4,10,'x',markeredgecolor='m',markerfacecolor='none')
# ####

# #### rnom=1.5 tstuck
# ax.plot(3,30,'o',markeredgecolor='b',markerfacecolor='none')
# ax.plot(3,31,'^',markeredgecolor='g',markerfacecolor='none')
# ax.plot(3,40,'s',markeredgecolor='r',markerfacecolor='none')
# ax.plot(3,41,'d',markeredgecolor='c',markerfacecolor='none')
# ax.plot(3,55,'x',markeredgecolor='m',markerfacecolor='none')

# ax.plot(3.5,45,'o',markeredgecolor='b',markerfacecolor='none')
# ax.plot(3.5,45,'^',markeredgecolor='g',markerfacecolor='none')
# ax.plot(3.5,50,'s',markeredgecolor='r',markerfacecolor='none')
# ax.plot(3.5,60,'d',markeredgecolor='c',markerfacecolor='none')
# ax.plot(3.5,100,'x',markeredgecolor='m',markerfacecolor='none')

# ax.plot(4,65,'o',markeredgecolor='b',markerfacecolor='none')
# ax.plot(4,75,'^',markeredgecolor='g',markerfacecolor='none')
# ax.plot(4,85,'s',markeredgecolor='r',markerfacecolor='none')
# ax.plot(4,95,'d',markeredgecolor='c',markerfacecolor='none')
# ax.plot(4,160,'x',markeredgecolor='m',markerfacecolor='none') # meaningless noise at 80..
# ####

# #### rnom=1.5 nstuck
# ax.plot(3,30,'o',markeredgecolor='b',markerfacecolor='none')
# ax.plot(3,34,'^',markeredgecolor='g',markerfacecolor='none')
# ax.plot(3,40,'s',markeredgecolor='r',markerfacecolor='none')
# ax.plot(3,50,'d',markeredgecolor='c',markerfacecolor='none')
# ax.plot(3,60,'x',markeredgecolor='m',markerfacecolor='none')

# ax.plot(3.5,45,'o',markeredgecolor='b',markerfacecolor='none')
# ax.plot(3.5,50,'^',markeredgecolor='g',markerfacecolor='none')
# ax.plot(3.5,70,'s',markeredgecolor='r',markerfacecolor='none')
# ax.plot(3.5,85,'d',markeredgecolor='c',markerfacecolor='none')
# ax.plot(3.5,85,'x',markeredgecolor='m',markerfacecolor='none')

# ax.plot(4,70,'o',markeredgecolor='b',markerfacecolor='none')
# ax.plot(4,80,'^',markeredgecolor='g',markerfacecolor='none')
# ax.plot(4,105,'s',markeredgecolor='r',markerfacecolor='none')
# ax.plot(4,125,'d',markeredgecolor='c',markerfacecolor='none')
# ax.plot(4,130,'x',markeredgecolor='m',markerfacecolor='none')
# ####

# Axes
ax.axis([rmin,rmax,1,200])
ax.set_xlabel(r"Effective Radius of the Cavity, $\mathbf{r_{\mathbf{eff}}}$",fontsize=textsize)
#ax.set_ylabel(r"Critical Polymer Length, $\mathbf{N^*}$",fontsize=textsize)
ax.set_ylabel(r"$\mathbf{N}$ at $\mathbf{min(t_{\mathbf{stuck}})}$",fontsize=textsize)
#ax.set_ylabel(r"$\mathbf{N}$ at $\mathbf{max(N_{\mathbf{stuck}})}$",fontsize=textsize)

## Plot models
r_effs = np.arange(rmin,rmax,0.1)
#ax.plot(r_effs, CalcNpacked_rand(r_effs), 'm-', linewidth=3, label='Random Dense Packing')
ax.plot(r_effs, CalcNfree_RG(r_effs),   'b-', linewidth=3, label='Free Polymer, R_G')
ax.plot(r_effs, CalcNcrit(r_effs,0.4),'b--',linewidth=3)
ax.plot(r_effs, CalcNcrit(r_effs,0.5),'g--',linewidth=3)
ax.plot(r_effs, CalcNcrit(r_effs,0.6),'y--',linewidth=3)
ax.plot(r_effs, CalcNcrit(r_effs,0.75),'r--',linewidth=3)
ax.plot(r_effs, CalcNcrit(r_effs,1.0),'c--',linewidth=3)
ax.plot(r_effs, CalcNcrit(r_effs,1.5),'m--',linewidth=3)

## Plot dilute regime limit
# N (4/3) pi r^3 = phi * ( pi R^2 2 R )
phi = 0.15
ax.plot(r_effs, phi*12*r_effs**3, 'k:', linewidth=5)

## Labels
#ax.text(2.52,182,'Random Dense Packing',size=textsize,color='m',rotation=28)
ax.text(3.52,37,r'Free Polymer, $\mathbf{R_G=r_{\mathbf{eff}}}$',size=textsize,color='b',rotation=10)

opt = dict(color='k',width=1)
#ax.annotate(r'$\phi = 0.20$',xytext=(3.05,105),xy=(3.4,95),size=textsize,arrowprops=opt)
# ### rnom=1.5
# ax.annotate(r'$\phi = 0.15$',xytext=(3.2,105),xy=(3.4,75),size=textsize,arrowprops=opt)

# therotation = 18
# ax.text(3.0,83,'F = 1.5',size=textsize,color='m',rotation=therotation)
# ax.text(3.1,67,'F = 1.0',size=textsize,color='c',rotation=therotation)
# ax.text(3.2,60,'F = 0.75',size=textsize,color='r',rotation=therotation)
# ax.text(3.3,48,'F = 0.5',size=textsize,color='g',rotation=therotation)
# ax.text(3.55,50,'F = 0.4',size=textsize,color='b',rotation=therotation)

### rnom=1.3
ax.annotate(r'$\phi = 0.15$',xytext=(3.0,115),xy=(3.4,75),size=textsize,arrowprops=opt)

therotation = 18
## nu=0.588 
ax.text(3.0,77,'F = 1.5',size=textsize,color='m',rotation=therotation)
ax.text(3.1,62,'F = 1.0',size=textsize,color='c',rotation=therotation)
ax.text(3.2,56,'F = 0.75',size=textsize,color='r',rotation=therotation)
ax.text(3.3,51,'F = 0.6',size=textsize,color='y',rotation=therotation)
ax.text(3.4,48,'F = 0.5',size=textsize,color='g',rotation=therotation)
#ax.text(3.55,55,'F = 0.5',size=textsize,color='g',rotation=therotation)
ax.text(3.55,46,'F = 0.4',size=textsize,color='b',rotation=therotation)

# # ## nu=0.628
# ax.text(3.0,87,'F = 1.5',size=textsize,color='m',rotation=therotation)
# ax.text(3.1,68,'F = 1.0',size=textsize,color='c',rotation=therotation)
# ax.text(3.2,59,'F = 0.75',size=textsize,color='r',rotation=therotation)
# ax.text(3.3,53,'F = 0.6',size=textsize,color='y',rotation=therotation)
# ax.text(3.4,49,'F = 0.5',size=textsize,color='g',rotation=therotation)
# #ax.text(3.55,55,'F = 0.5',size=textsize,color='g',rotation=therotation)
# ax.text(3.55,46,'F = 0.4',size=textsize,color='b',rotation=therotation)


## Axis ticks
ax.set_xlim([2.9,4.2])
ax.set_ylim([25,150])
ax.set_xscale('log')
ax.set_yscale('log')
ax.set_xticks(np.arange(3.0, 4.5, 0.5))
ax.get_xaxis().get_major_formatter().labelOnlyBase = False
ax.get_xaxis().set_major_formatter(FormatStrFormatter('%.1f'))
ax.set_yticks(np.arange(25,175,25.))
ax.get_yaxis().get_major_formatter().labelOnlyBase = False
ax.get_yaxis().set_major_formatter(FormatStrFormatter('%d'))
#ax.grid(True)
#ax.tick_params(labelsize=textsize)


## Show plot
plt.subplots_adjust(left=0.10, bottom=0.12, right=0.96, top=0.96,
                            wspace=0.2, hspace=0.2)
matplotlib.rcParams.update({'font.size': textsize})
plt.show()
