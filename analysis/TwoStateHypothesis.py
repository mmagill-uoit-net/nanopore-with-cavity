import numpy as np
import matplotlib.pyplot as plt


N = np.arange(300)
Nstar = 150

# Behaviour for N>Nstar is probably wrong but doesn't matter much
# Want a plateau near Nstar
tauNT = (N-Nstar)**2

# Behaviour for N<Nstar is definitely wrong but doesn't matter much
# Much bigger than tauNT at Nstar
tauT = tauNT[0]

# By definition
NstkNT = N

# Should be undefined for N<<Nstar, but doesn't matter
NstkT = Nstar*0.8

# Transition over ~10 Ns
PT = 0.5 * (np.tanh((N-Nstar)/10.) + 1)
plt.plot(N,PT)
plt.xlabel("N")
plt.ylabel("Hypothesized Probability of Having a Tail")
plt.show()

# ttrans
meantau = tauNT*(1-PT) + tauT*PT
plt.plot(N,meantau)
plt.xlabel("N")
plt.ylabel("Average ttrans")
plt.text(Nstar*0.5,tauT,"min(ttrans) at N=%d"%N[np.argmin(meantau)])
plt.show()

# Nstuck
meanNstk = NstkNT*(1-PT) + NstkT*PT
plt.plot(N,meanNstk)
plt.xlabel("N")
plt.ylabel("Average Nstuck")
plt.text(Nstar,NstkT*0.5,"min(Nstuck) at N=%d"%N[np.argmax(meanNstk)])
plt.show()
